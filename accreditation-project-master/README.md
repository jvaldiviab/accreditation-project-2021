# PROYECTO DE ACREDITACION
## EQUIPOS DE ACREDITACION
### EQUIPO ACTUAL DE ACREDITACION 2021
* **Rondon Polanco, Sergio Rolan** - *Project Leader* -
* **Atamari Aguilar, Joel Jason** - *Test Leader* -
* **Cayro Mamani, Alexander Rey** - *Backend Leader* -
* **Mamani Mamani, Jhon** - *Frontend Leader* -
* **Mendizabal Alpaca, Renato Alonso** - *Risk Analyst* -
* **Suri Canazas, Jose Manuel** - *Configuration Management Leader * -
* **Uraccahua Barrios, Hebert** - *Project Member* -
* **Valdivia Berrios, Juan Carlos** - *Quality Manager* -

### EQUIPO DE ACREDITACION 2020 
    GRUPO 3: Acreditacion
    Ahuate Toribio, Cristian
    Champi Paredes, Brigitte
    Cruces Ramos, Danilo
    Manchego Machaca, Rodolfo
    Ortiz Chavez, Jesus
    Valdivia Baldarrago, Christian
    Zea Quispe, Gerald
