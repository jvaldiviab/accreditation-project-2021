from apps.user.apps import UserConfig
from apps.process.apps import ProcessConfig
from apps.portfolio.apps import PortfolioConfig

class UserAppConfig(UserConfig):
    name = 'apps.user'
    verbose_name = 'usuario'

class ProcessAppConfig(ProcessConfig):
    name = 'apps.process'
    verbose_name = 'proceso'

class PortfolioAppConfig(PortfolioConfig):
    name = 'apps.portfolio'
    verbose_name = 'portafolio'