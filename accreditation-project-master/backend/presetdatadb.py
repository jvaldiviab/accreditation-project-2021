import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "accreditation.settings")
import django
django.setup()
from random import randrange, choice
from apps.user.models import *
from apps.process.models import *
import csv

with open('datasets/usuarios_epis.csv',encoding='ISO-8859-1') as file: USERS_EPIS=[row for row in csv.reader(file)]
with open('datasets/cursos_epis_plan2013.csv',encoding='ISO-8859-1') as file: COURSES_EPIS_PLAN2013=[row for row in csv.reader(file)]
with open('datasets/cursos_epis_plan2017.csv',encoding='ISO-8859-1') as file: COURSES_EPIS_PLAN2017=[row for row in csv.reader(file)]
with open('datasets/cursos_epis_plan2021.csv',encoding='ISO-8859-1') as file: COURSES_EPIS_PLAN2021=[row for row in csv.reader(file)]

# ──────────────────────────────────────────────────
PROCESS_EPIS = 'acreditación epis'
PROCESS = (PROCESS_EPIS,'acreditación epie','acreditación epit')
P_PHASES = ('auto evaluación','acreditación','mejora continua') 
P_P_CRITERIAS = ('criterio 1','criterio 2','criterio 3',)
# ──────────────────────────────────────────────────
FORMATION_AREAS = ('matemáticas y ciencias básicas','educación general','tópicos de ingeniería')
SUBAREAS = ('computación','investigación','ingeniería')
DEPARTMENT = (
    ('SI','sistemas de información'),
    ('CC','ciencias de la computación'),
    ('II','innovación e investigación'),
    ('RH','recursos humanos'),
    ('PG','postgrado'),
    ('PR','pregrado'),
    ('MA','matemática'),
    ('QI','química'),
    ('FI','física'),
)
# ──────────────────────────────────────────────────
P_PLAN2013 = ('plan de estudio 2013',)
P_PLAN2017 = ('plan de estudio 2017',)
P_PLAN2021 = ('plan de estudio 2021',)
P_RESOURCES = (P_PLAN2013,P_PLAN2017,P_PLAN2021)
P_R_STUDENTRESULT_NUM = 5
P_R_SR_CRITERIA_NUM = 3
P_R_COMPETENCE_NUM = 5
P_R_COURSES = {(6849573,'curso física'),(7654895,'curso química'),(8956486,'curso matemática')}

if __name__ == '__main__':

# USUARIOS SIN ROL ──────────────────────────────────────────────────
    '''Primero se crean los usuarios sin rol, ya que los roles aún no están creados, ya que pertenecen a los procesos y estos tampoco existen'''
    for u in USERS_EPIS[1:]:
        if not User.objects.filter(email=u[0]).exists():
            user = User.objects.create(email=u[0],first_name=u[1],last_name=u[2],superadmin=u[5])
            user.set_password(u[3])
            user.save()

# PROCESOS, ROLES, FASES, CRITERIOS ──────────────────────────────────────────────────
    '''El sistema debe tener al menos un usuario superadministrador para crear procesos'''
    for pr in PROCESS:
        # process = Process.objects.filter(name=pr)[0]
        if not Process.objects.filter(name=pr).exists():
            superadmins = User.objects.filter(superadmin=True)
            if not superadmins.exists(): break
            process = Process.objects.create(name=pr,id_owner=superadmins[0],id_user=superadmins[0])
            for ro in Role.ROLES:
                role = Role.objects.get_or_create(id_process=process,name=ro)[0]
                for pm in Permission.MODULES:
                    for pp in Permission.PERMISSIONS:
                        if (role.name,pp) == (Role.ADMINISTRATIVE,Permission.DELETE): continue
                        if role.name == Role.TEACHER and not ((pp,pm) == (Permission.READ,Permission.MODULE_USER)): continue
                        Permission.objects.get_or_create(id_role=role,permission=pp[0],module=pm[0])
            for ph in P_PHASES:
                phase = Phase.objects.get_or_create(id_process=process,name=ph)[0]
                for cr in P_P_CRITERIAS: Criteria.objects.get_or_create(id_phase=phase,name=cr)

# USUARIOS CON ROLES EPIS ──────────────────────────────────────────────────
    '''Después de haber creado los procesos y sus roles se les asigna a los usuarios existentes'''
    process_epis = Process.objects.get(name=PROCESS[0])
    for u in USERS_EPIS[1:]:
        user = User.objects.get(email=u[0])
        user.id_role = Role.objects.get_or_create(id_process=process_epis,name=u[4])[0]
        user.save()

# PROCESO, PORTAFOLIO, RECURSOS ──────────────────────────────────────────────────
    from apps.portfolio.models import *

    for fa in FORMATION_AREAS:
        f_area = FormationArea.objects.get_or_create(name=fa)[0]
        for sa in SUBAREAS: Subarea.objects.get_or_create(id_formation_area=f_area,name=sa)
    for cd in DEPARTMENT: Department.objects.get_or_create(code=cd[0],name=cd[1])

    for process in Process.objects.all():
        for rs in P_RESOURCES:
            if not Resource.objects.filter(id_process=process,name=rs[0]).exists():
                resource = Resource.objects.create(id_process=process,name=rs[0])
                resource.save()
                for sr in range(P_R_STUDENTRESULT_NUM):
                    s_result = StudentResult.objects.get_or_create(id_resource=resource,name=f're{sr+1}')[0]
                    for cr in range(P_R_SR_CRITERIA_NUM): Criteria.objects.get_or_create(id_student_result=s_result,name=f're{sr+1}.{cr+1}')[0]
                for cp in range(P_R_COMPETENCE_NUM): competence = Competence.objects.get_or_create(id_resource=resource,name=f'c{chr(cp+97)}')[0]
                COURSES_PLAN = P_R_COURSES if not process.name==PROCESS_EPIS else COURSES_EPIS_PLAN2013[1:] if rs==P_PLAN2013 else COURSES_EPIS_PLAN2017[1:] if rs==P_PLAN2017 else COURSES_EPIS_PLAN2021[1:]
                for cs in COURSES_PLAN:
                    if not Course.objects.filter(id_resource=resource,code=cs[0],name=cs[1]).exists():
                        imported = len(cs)>2
                        course = Course.objects.create(id_resource=resource,code=cs[0],name=cs[1],
                            id_subarea = choice(Subarea.objects.all()),
                            semester = cs[2] if imported else randrange(1,17),
                            credit = cs[3] if imported else randrange(1,6),
                            hours_theory = cs[6] if imported else randrange(8),
                            hours_seminar = cs[7] if imported else randrange(8),
                            hours_theopractice = cs[8] if imported else randrange(8),
                            hours_practice = cs[9] if imported else randrange(8),
                            hours_laboratory = cs[10] if imported else randrange(8)
                        )
                        course.departments.add(choice(Department.objects.all()))
                        for _ in range(P_R_STUDENTRESULT_NUM): course.student_results.add(choice(StudentResult.objects.filter(id_resource=resource)))
                        for _ in range(P_R_COMPETENCE_NUM): course.competences.add(choice(Competence.objects.filter(id_resource=resource)))
                        if imported: course.elective = cs[11]
                        course.save()