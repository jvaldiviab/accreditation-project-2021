from django.contrib import admin
from apps.process.models import *

'''Clases de los modelos de la aplicación de Procesos en administración'''
class ProcessAdmin(admin.ModelAdmin):
    ''''Clase principal de Procesos en administración'''
    list_display       = ('id','name','id_user','status')
    list_display_links = ('name',)
    list_filter        = ('status',)
    search_fields      = ('name',)
    readonly_fields    = ('id_owner','created','updated')
    list_per_page      = 25
    # list_editable    = ('id_user',)
    fieldsets = (
        (None,{'classes':('wide',),'fields':(
            'name','id_user','description',('started','closed'),
            'status','id_owner','created','updated'
        )}),
    )
    def save_model(self, request, obj, form, change):
        '''Función para guardar un registro del modelo en administración'''
        # se asigna automaticamente al usuario logeado como creador y responsable del proceso
        if not obj.id:
            obj.id_owner = request.user
            try: obj.id_user
            except: obj.id_user = request.user
        super().save_model(request, obj, form, change)

class PhaseAdmin(admin.ModelAdmin):
    ''''Clase principal de Fases en administración'''
    list_display       = ('id','name','id_process','status')
    list_display_links = ('name',)
    list_filter        = ('status','id_process')
    search_fields      = ('name',)
    readonly_fields    = ('created','updated')
    list_per_page      = 25

class CriteriaAdmin(admin.ModelAdmin):
    ''''Clase principal de Criterios en administración'''
    list_display       = ('id','name','phase','process','id_user')
    list_display_links = ('name',)
    list_filter        = ('status','id_phase__id_process')
    search_fields      = ('name',)
    readonly_fields    = ('created','updated')
    list_per_page      = 25
    def phase(self, obj): return obj.id_phase.name
    phase.short_description = 'fase'
    def process(self, obj): return obj.id_phase.id_process
    process.short_description = 'proceso'

# REGISTROS ═══════════════════════════════════════════════════════════════════════════════════════
'''Registro de los modelos de la aplicación de Procesos'''
admin.site.register(Process,ProcessAdmin)
admin.site.register(Phase,PhaseAdmin)
admin.site.register(Criteria,CriteriaAdmin)