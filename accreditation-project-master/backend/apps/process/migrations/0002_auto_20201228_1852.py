# Generated by Django 3.0.8 on 2020-12-28 23:52

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('process', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='id_user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='responsable'),
        ),
        migrations.AddField(
            model_name='process',
            name='id_owner',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.PROTECT, related_name='process_created', to=settings.AUTH_USER_MODEL, verbose_name='creado por'),
        ),
        migrations.AddField(
            model_name='process',
            name='id_user',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.PROTECT, related_name='process_managed', to=settings.AUTH_USER_MODEL, verbose_name='responsable'),
        ),
        migrations.AddField(
            model_name='phase',
            name='id_process',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='process.Process', verbose_name='proceso'),
        ),
        migrations.AddField(
            model_name='indicator',
            name='id_criteria',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='process.Criteria', verbose_name='criterio'),
        ),
        migrations.AddField(
            model_name='indicator',
            name='users',
            field=models.ManyToManyField(blank=True, to=settings.AUTH_USER_MODEL, verbose_name='responsables'),
        ),
        migrations.AddField(
            model_name='criteria',
            name='id_phase',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='process.Phase', verbose_name='fase'),
        ),
        migrations.AddField(
            model_name='criteria',
            name='id_user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='responsable'),
        ),
        migrations.AlterUniqueTogether(
            name='task',
            unique_together={('id_indicator', 'name')},
        ),
        migrations.AlterUniqueTogether(
            name='phase',
            unique_together={('id_process', 'name')},
        ),
        migrations.AlterUniqueTogether(
            name='indicator',
            unique_together={('id_criteria', 'name')},
        ),
        migrations.AlterUniqueTogether(
            name='criteria',
            unique_together={('id_phase', 'name')},
        ),
    ]
