from apps.process.models import Process
from apps.user.models import User
from datetime import date, datetime
from django.contrib.postgres.fields import ArrayField
from django.core.validators import RegexValidator, FileExtensionValidator, MinValueValidator, MaxValueValidator
from django.db import models
from multiselectfield import MultiSelectField

def get_filename(instance): return instance.name.split("/")[-1] if instance else None

def CourseActive_path(CourseActive): return f'documents/{CourseActive.id_portfolio.semester_complete()}/{CourseActive.id_course.name}'
'''Función para obtener la dirección donde se almacena los documentos de un curso activo'''

def silabus_dufa_path(instance, filename): return f'{CourseActive_path(instance)}/{filename.upper()}'
'''Función para obtener la dirección donde se almacena el silabo dufa de un curso activo'''

def EntranceExamination_path(instance, filename): return f'{CourseActive_path(instance.id_course_active)}/EXAMEN_ENTRADA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de examen de entrada de un curso activo'''

def EvidenceDocument_path(instance, filename): return f'{CourseActive_path(instance.id_period_academic.id_course_active)}/EVIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de evidencias de un curso activo'''

def incidence_path(instance, filename): return f'{CourseActive_path(instance.id_student.id_course_active)}/INCIDENCIA/{filename.upper()}'
'''Función para obtener la dirección donde se almacena los documentos de incidencia de un curso activo'''

def template_path(instance, filename): return f'documents/templates/{filename.upper()}'
'''Función para obtener la dirección donde se almacena las plantillas de los recursos de portafolio'''

# GENERAL ═════════════════════════════════════════════════════════════════════════════════════════

class FormationArea(models.Model):
    '''Modelo de áreas de formación en la base de datos'''
    name = models.CharField(blank=False,unique=True,max_length=32,verbose_name='nombre',
        validators = [RegexValidator(regex=r'^[\da-záéíóúñ\s]{2,32}$',
            message = 'Solo se admite la siguiente validación: [a-z][0-9][á-úñ] y espacios, de 2 a 32 caracteres'
    )]) # nombre del área de formación
    status = models.BooleanField(blank=True,default=True,verbose_name='estado')
    # estado del área de formación
    created = models.DateTimeField(auto_now_add=True,verbose_name='fecha de creación')
    # fecha de creación de los datos (automático)
    updated = models.DateTimeField(auto_now=True,verbose_name='fecha de actualización')
    # fecha de actualización de los datos (automático)

    class Meta:
        verbose_name = 'área de formación'
        verbose_name_plural = 'general: áreas de formación'

    def __str__(self): return f'{self.name}'

class Subarea(models.Model):
    '''Modelo de subáreas de formación en la base de datos'''
    id_formation_area = models.ForeignKey(FormationArea,on_delete=models.CASCADE,null=False,blank=False,verbose_name='área de formación')
    # id del área de formación al que pertenece la subárea
    name = models.CharField(blank=False,max_length=32,verbose_name='nombre',
        validators = [RegexValidator(regex=r'^[\da-záéíóúñ\s]{2,32}$',
            message = 'Solo se admite la siguiente validación: [a-z][0-9][á-úñ] y espacios, de 2 a 32 caracteres'
    )]) # nombre de la subárea de formación
    status = models.BooleanField(blank=True,default=True,verbose_name='estado')
    # estado de la subárea de formación

    class Meta:
        verbose_name = 'subárea'
        verbose_name_plural = 'subáreas'
        unique_together = ['id_formation_area', 'name']

    def __str__(self): return f'{self.name} | {self.id_formation_area}'

class Department(models.Model):
    '''Modelo de departamentos en la base de datos'''
    name = models.CharField(blank=False,unique=True,max_length=32,verbose_name='nombre',
        validators = [RegexValidator(regex=r'^[\da-záéíóúñ\s]{2,32}$',
            message = 'Solo se admite la siguiente validación: [a-z][0-9][á-úñ] y espacios, de 2 a 32 caracteres'
    )]) # nombre del departamento
    code = models.CharField(blank=False,max_length=2,verbose_name='código',
        validators = [RegexValidator(regex=r'^[A-Z]{2}$',
            message = 'Solo se admite la siguiente validación: [A-Z], de 2 caracteres'
    )]) # código de caracteres del departamento
    status = models.BooleanField(blank=True,default=True,verbose_name='estado')
    # estado del departamento
    created = models.DateTimeField(auto_now_add=True,verbose_name='fecha de creación')
    # fecha de creación de los datos (automático)
    updated = models.DateTimeField(auto_now=True,verbose_name='fecha de actualización')
    # fecha de actualización de los datos (automático)

    class Meta:
        verbose_name = 'departamento'
        verbose_name_plural = 'general: departamentos'
        unique_together = ['name', 'code']

    def __str__(self): return f'{self.name}'

class Template(models.Model):
    '''Modelo de las plantillas en la base de datos'''
    template = models.FileField(upload_to=template_path,null=False,blank=False,unique=True,verbose_name='plantilla',
        validators = [FileExtensionValidator(allowed_extensions=['pdf','docx','xlsx'],
            message = 'Solo se aceptan los siguientes formatos: pdf,docx,xlsx'
    )]) # archivo de la plantilla

    class Meta:
        verbose_name = 'plantilla'
        verbose_name_plural = 'general: plantillas'

    def __str__(self): return f'[{self.id}] plantilla: {self.template}'

    def get_filename(self): return self.template.name.split("/")[-1]
    get_filename.short_description = 'nombre'

# RECURSOS ════════════════════════════════════════════════════════════════════════════════════════

class Resource(models.Model):
    '''Modelo de recursos (plan de estudio) para el portafolio en la base de datos'''
    id_process = models.ForeignKey(Process,on_delete=models.CASCADE,null=False,blank=False,verbose_name='proceso')
    # id de proceso al que pertenece el recurso
    name = models.CharField(blank=False,max_length=32,verbose_name='nombre',
        validators = [RegexValidator(regex=r'^[\da-záéíóúñ\s]{2,32}$',
            message = 'Solo se admite la siguiente validación: [a-z][0-9][á-úñ] y espacios, de 2 a 32 caracteres'
    )]) # nombre del recurso (plan de estudio)
    description = models.TextField(blank=True,max_length=100,verbose_name='descripción',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,100}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 100 caracteres'
    )]) # descripción del recurso
    percent_due = models.PositiveSmallIntegerField(blank=True,default=75,verbose_name='porcentaje esperado',
        validators = [MaxValueValidator(100)],
    ) # porcentaje esperado de resultado de evaluación
    status = models.BooleanField(blank=True,default=True,verbose_name='estado')
    # estado del recurso
    created = models.DateTimeField(auto_now_add=True,verbose_name='fecha de creación')
    # fecha de creación de los datos (automático)
    updated = models.DateTimeField(auto_now=True,verbose_name='fecha de actualización')
    # fecha de actualización de los datos (automático)

    class Meta:
        verbose_name = 'recurso (plan de estudio)'
        verbose_name_plural = 'recursos (planes de estudio)'
        unique_together = ['id_process', 'name']

    def __str__(self): return f'[{self.id}] {self.name} | {self.id_process}'

# RESULTADOS DEL ESTUDIANTE ───────────────────────────────────────────────────

class StudentResult(models.Model):
    '''Modelo de resultados del estudiante de recursos (plan de estudio) en la base de datos'''
    id_resource = models.ForeignKey(Resource,on_delete=models.CASCADE,null=False,blank=False,verbose_name='recurso')
    # id del recurso (plan de estudio) al que pertenece el resultado de estudiante
    name = models.CharField(blank=False,max_length=32,verbose_name='nombre',
        validators = [RegexValidator(regex=r'^[\da-záéíóúñ.,;:]{2,32}$',
            message = 'Solo se admite la siguiente validación: [a-z][0-9][á-úñ][.,::], de 2 a 32 caracteres'
    )]) # nombre del resultado del estudiante
    description = models.TextField(blank=True,max_length=420,verbose_name='descripción',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,420}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 420 caracteres'
    )]) # descripción del resultado del estudiante
    status = models.BooleanField(blank=True,default=True,verbose_name='estado')
    # estado del resultado del estudiante
    created = models.DateTimeField(auto_now_add=True,verbose_name='fecha de creación')
    # fecha de creación de los datos (automático)
    updated = models.DateTimeField(auto_now=True,verbose_name='fecha de actualización')
    # fecha de actualización de los datos (automático)

    class Meta:
        verbose_name = 'resultado de estudiante del recurso'
        verbose_name_plural = 'recursos: resultados de estudiante'
        unique_together = ['id_resource', 'name']

    def __str__(self): return f'[{self.id}] {self.name} | {self.id_resource}'

class Criteria(models.Model):
    '''Modelo de criterios de resultados de estudiante en la base de datos'''
    LEVEL0 = (0,'conoce')
    LEVEL1 = (1,'comprende')
    LEVEL2 = (2,'aplica en un nivel intermedio')
    LEVEL3 = (3,'logra el resultado del estudiante')
    LEVELS = (LEVEL0,LEVEL1,LEVEL2,LEVEL3)

    id_student_result = models.ForeignKey(StudentResult,on_delete=models.CASCADE,null=False,blank=False,verbose_name='resultado del estudiante')
    # id del resultado de estudiante al que pertenece el criterio
    name = models.CharField(blank=False,max_length=32,verbose_name='nombre',
        validators = [RegexValidator(regex=r'^[\da-záéíóúñ.,;:]{2,32}$',
            message = 'Solo se admite la siguiente validación: [a-z][0-9][á-úñ][.,::], de 2 a 32 caracteres'
    )]) # nombre del criterio
    description = models.TextField(blank=True,max_length=256,verbose_name='descripción',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,256}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 256 caracteres'
    )]) # descripción del criterio
    level1 = models.TextField(blank=True,max_length=156,verbose_name='nivel 1',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,156}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 156 caracteres'
    )]) # descripción del nivel 1 (insatisfactorio 25%) del criterio
    level2 = models.TextField(blank=True,max_length=156,verbose_name='nivel 2',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,156}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 156 caracteres'
    )]) # descripción del nivel 2 (en proceso 50%) del criterio
    level3 = models.TextField(blank=True,max_length=156,verbose_name='nivel 3',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,156}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 156 caracteres'
    )]) # descripción del nivel 3 (satisfactorio 75%) del criterio
    level4 = models.TextField(blank=True,max_length=156,verbose_name='nivel 4',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,156}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 156 caracteres'
    )]) # descripción del nivel 4 (sobresaliente 100%) del criterio
    level_suggested = MultiSelectField(choices=LEVELS,default=list,max_length=7,max_choices=4,verbose_name='nivel sugerido',
        validators = [RegexValidator(regex=r'^[0123,]{7}$',
            message = 'Solo se admite los caracteres [0123,]'
    )]) # nivel sugerido del criterio
    status = models.BooleanField(blank=True,default=True,verbose_name='estado')
    # estado del criterio

    class Meta:
        verbose_name = 'criterio del resultado de estudiante'
        verbose_name_plural = 'criterios del resultado de estudiante'
        unique_together = ['id_student_result','name']

    def __str__(self): return f'[{self.id}] {self.name} | {self.id_student_result}'

# COMPETENCIAS ────────────────────────────────────────────────────────────────

class Competence(models.Model):
    '''Modelo de competencias de recursos (plan de estudio) en la base de datos'''
    id_resource = models.ForeignKey(Resource,on_delete=models.CASCADE,null=False,blank=False,verbose_name='recurso')
    # id del recurso (plan de estudio) al que pertenece la competencia
    name = models.CharField(blank=False,max_length=32,verbose_name='nombre',
        validators = [RegexValidator(regex=r'^[\da-záéíóúñ.,;:\s]{2,32}$',
            message = 'Solo se admite la siguiente validación: [a-z][0-9][á-úñ][.,::] y espacios, de 2 a 32 caracteres'
    )]) # nombre de la competencia
    description = models.TextField(blank=True,max_length=320,verbose_name='descripción',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,320}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 320 caracteres'
    )]) # descripción de la competencia
    status = models.BooleanField(blank=True,default=True,verbose_name='estado')
    # estado de la competencia
    created = models.DateTimeField(auto_now_add=True,verbose_name='fecha de creación')
    # fecha de creación de los datos (automático)
    updated = models.DateTimeField(auto_now=True,verbose_name='fecha de actualización')
    # fecha de actualización de los datos (automático)

    student_results = models.ManyToManyField(StudentResult,blank=True,related_name='competences',verbose_name='resultados del estudiante')
    # conjunto de id's de resultados de estudiante de la competencia

    class Meta:
        verbose_name = 'competencia del recurso'
        verbose_name_plural = 'recursos: competencias'
        unique_together = ['id_resource', 'name']

    def __str__(self): return f'[{self.id}] {self.name} | {self.id_resource}'

# CURSOS ──────────────────────────────────────────────────────────────────────

class Course(models.Model):
    '''Modelo de cursos de malla de recursos (plan de estudio) en la base de datos'''
    TYPES = ((0, 'control'), (1, 'capstone'))

    id_resource = models.ForeignKey(Resource,on_delete=models.CASCADE,null=False,blank=False,verbose_name='recurso')
    # id del recurso (plan de estudio) al que pertenece el curso
    id_subarea = models.ForeignKey(Subarea,on_delete=models.CASCADE,null=False,blank=False,verbose_name='subárea de formación')
    # id de la subárea de formación al que pertenece el curso
    code = models.PositiveIntegerField(blank=False,verbose_name='código',
        validators = [MinValueValidator(1000000), MaxValueValidator(99999999)]
    ) # código del curso
    name = models.CharField(blank=False,max_length=100,verbose_name='nombre',
        validators = [RegexValidator(regex=r'^[\da-záéíóúñ\-_.,;:"#%&~^/\\(){}¡!¿?\s]{3,100}$',
            message = 'Solo se admite la siguiente validación: [a-z][0-9][á-úñÑ][-_.,:;"#%&~^/\\(){}!¡¿?] y espacios, de 3 a 100 caracteres'
    )]) # nombre del curso
    description = models.TextField(blank=True,max_length=1000,verbose_name='descripción',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,1000}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\(){}!¡¿?] y espacios, hasta 1000 caracteres'
    )]) # descripción del curso
    content = models.TextField(blank=True,max_length=4000,verbose_name='contenido',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,4000}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\(){}!¡¿?] y espacios, hasta 4000 caracteres'
    )]) # contenido del curso
    semester = models.PositiveSmallIntegerField(blank=True,default=1,verbose_name='semestre',
        validators = [MinValueValidator(1), MaxValueValidator(16)]
    ) # semestre del curso
    credit = models.PositiveSmallIntegerField(blank=True,default=1,verbose_name='créditos',
        validators = [MaxValueValidator(5)]
    ) # numero de créditos del curso
    hours_theory = models.PositiveSmallIntegerField(blank=True,default=0,verbose_name='horas de teoría',
        validators = [MaxValueValidator(8)]
    ) # horas de teoría del curso
    hours_seminar = models.PositiveSmallIntegerField(blank=True,default=0,verbose_name='horas de seminario',
        validators = [MaxValueValidator(8)]
    ) # horas de seminario del curso
    hours_theopractice = models.PositiveSmallIntegerField(blank=True,default=0,verbose_name='horas de teórico-practico',
        validators = [MaxValueValidator(8)]
    ) # horas teórico practicas del curso
    hours_practice = models.PositiveSmallIntegerField(blank=True,default=0,verbose_name='horas de practica',
        validators = [MaxValueValidator(8)]
    ) # horas de practicas del curso
    hours_laboratory = models.PositiveSmallIntegerField(blank=True,default=0,verbose_name='horas de laboratorio',
        validators = [MaxValueValidator(8)]
    ) # horas de laboratorio del curso
    type = models.PositiveSmallIntegerField(blank=True,default=1,choices=TYPES,verbose_name='tipo de curso',
        validators = [MaxValueValidator(1)]
    ) # tipo de curso
    elective = models.BooleanField(blank=True,default=False,verbose_name='electivo')
    # estado electivo del curso
    status = models.BooleanField(blank=True,default=True,verbose_name='estado')
    # estado del curso
    created = models.DateTimeField(auto_now_add=True,verbose_name='fecha de creación')
    # fecha de creación de los datos (automático)
    updated = models.DateTimeField(auto_now=True,verbose_name='fecha de actualización')
    # fecha de actualización de los datos (automático)

    departments = models.ManyToManyField(Department,blank=True,related_name='courses',verbose_name='departamentos')
    # conjunto de id's de los departamentos del curso
    pre_requirements = models.ManyToManyField('self',blank=True,symmetrical=False,related_name='post_requirements',verbose_name='pre-requisitos')
    # conjunto de id's de los cursos pre-requisitos del curso
    student_results = models.ManyToManyField(StudentResult,through='Course_StudentResult',blank=True,related_name='courses',verbose_name='resultados del estudiante')
    # conjunto de id's de los resultados de estudiante del curso
    competences = models.ManyToManyField(Competence,through='Course_Competence',blank=True,related_name='courses',verbose_name='competencias')
    # conjunto de id's de competencias del curso

    class Meta:
        verbose_name = 'curso del recurso (plan de estudio)'
        verbose_name_plural = 'recursos: cursos'
        unique_together = ['id_resource','code']

    def __str__(self): return f'[{self.id}] {self.name} | {self.id_resource}'
    # def get_required(self): return self.pre_requirements.through.objects.filter(to_course=self).exists()
    def get_required(self): return self.post_requirements.exists()

class Course_StudentResult(models.Model):
    '''Modelo de los cursos por resultados de estudiante en la base de datos'''
    id_course = models.ForeignKey(Course,on_delete=models.CASCADE,null=False,blank=False,verbose_name='curso')
    # id del curso
    id_student_result = models.ForeignKey(StudentResult,on_delete=models.CASCADE,null=False,blank=False,verbose_name='resultado de estudiante')
    # id del resultado de estudiante
    level = models.PositiveSmallIntegerField(blank=True,default=Criteria.LEVEL0[0],choices=Criteria.LEVELS,verbose_name='nivel',
        validators = [MaxValueValidator(3)]
    ) # nivel elegido del curso por resultado del estudiante

    class Meta:
        verbose_name = 'resultado de estudiante del curso'
        verbose_name_plural = 'resultados de estudiante del curso'

    def __str__(self): return f'[{self.id}] {self.id_course} ── {self.id_student_result}'

class Course_Competence(models.Model):
    '''Modelo de los cursos por competencias en la base de datos'''
    id_course = models.ForeignKey(Course,on_delete=models.CASCADE,null=False,blank=False,verbose_name='curso')
    # id del curso
    id_competence = models.ForeignKey(Competence,on_delete=models.CASCADE,null=False,blank=False,verbose_name='competencia')
    # id de la competencia

    class Meta:
        verbose_name = 'competencia del curso'
        verbose_name_plural = 'competencias del curso'

    def __str__(self): return f'[{self.id}] {self.id_course} ── {self.id_competence}'

# PORTAFOLIO ══════════════════════════════════════════════════════════════════════════════════════

class Portfolio(models.Model):
    '''Modelo de portafolios por semestre en la base de datos'''
    SEMESTERS = ((0,'A'),(1,'B'),(2,'C'))

    id_process = models.ForeignKey(Process,on_delete=models.CASCADE,null=False,blank=False,verbose_name='proceso')
    # id del proceso al que pertenece el portafolio
    id_user = models.ForeignKey(User,on_delete=models.PROTECT,null=False,blank=False,verbose_name='responsable')
    # id del usuario responsable del portafolio
    semester = models.PositiveSmallIntegerField(choices=SEMESTERS,blank=False,verbose_name='semestre',
        validators = [MaxValueValidator(2)]
    ) # semestre del portafolio
    year = models.PositiveSmallIntegerField(blank=False,verbose_name='año',
        validators = [MinValueValidator(2000),MaxValueValidator(2050)]
    ) # año de el portafolio
    start_date = models.DateField(null=False,blank=False,verbose_name='fecha de inicio')
    # fecha de inicio de los cursos activos del portafolio
    end_phase1 = models.DateField(null=False,blank=False,verbose_name='cierre de la primera etapa')
    # fecha de cierre de la primera fase del portafolio
    end_phase2 = models.DateField(null=False,blank=False,verbose_name='cierre de la segunda etapa')
    # fecha de cierre de la segunda fase del portafolio
    end_phase3 = models.DateField(null=False,blank=False,verbose_name='cierre de la tercera etapa')
    # fecha de cierre de la tercera fase del portafolio
    end_phase4 = models.DateField(null=False,blank=False,verbose_name='cierre de la cuarta etapa')
    # fecha de cierre de la cuarta fase del portafolio
    status = models.BooleanField(blank=True,default=True,verbose_name='estado')
    # estado del portafolio
    created = models.DateTimeField(auto_now_add=True,verbose_name='fecha de creación')
    # fecha de creación de los datos (automático)
    updated = models.DateTimeField(auto_now=True,verbose_name='fecha de actualización')
    # fecha de actualización de los datos (automático)

    users = models.ManyToManyField(User,blank=True,related_name='portfolios',verbose_name='colaboradores/revisores')
    # conjunto de id's de los colaboradores/revisores del curso activo
    courses = models.ManyToManyField(Course,through='CourseActive',blank=False,related_name='portfolios',verbose_name='cursos')
    # conjunto de id's de cursos de malla

    class Meta:
        verbose_name = 'portafolio'
        verbose_name_plural = 'portafolios'
        unique_together = ['id_process','semester','year']

    def __str__(self): return f'[{self.id}] portafolio {self.semester_complete()} | {self.id_process}'

    def semester_complete(self): return f'{self.year}{self.get_semester_display()}'
    semester_complete.short_description = 'portafolio'

# CURSOS ACTIVOS ──────────────────────────────────────────────────────────────

class CourseActive(models.Model):
    '''Modelo de cursos activos del portafolio en la base de datos'''
    id_portfolio = models.ForeignKey(Portfolio,on_delete=models.CASCADE,null=False,blank=False,verbose_name='portafolio')
    # id del portafolio al que pertenece el curso activo
    id_course = models.ForeignKey(Course,on_delete=models.CASCADE,null=False,blank=False,verbose_name='curso')
    # id del recurso (curso de malla curricular)
    description = models.TextField(blank=True,max_length=1000,verbose_name='descripción',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\-.,;:"#%&~^/\\(){}¡!¿?\s]{0,1000}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][-_.,:;"#%&~^/\\(){}!¡¿?] y espacios, hasta 1000 caracteres'
    )]) # descripción del curso
    issues = models.TextField(blank=True,max_length=1000,verbose_name='asuntos a tratar',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\-.,;:"#%&~^/\\(){}¡!¿?\s]{0,1000}$',
            message = 'Solo se acepta la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][-_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 1000 caracteres.'
    )]) # asuntos a tratar del curso activo
    laboratories = models.TextField(blank=True,max_length=1000,verbose_name='laboratorios y practicas',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\-.,;:"#%&~^/\\(){}¡!¿?\s]{0,1000}$',
            message = 'Solo se acepta la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][-_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 1000 caracteres.'
    )]) # laboratorios del curso activo
    methodology = models.TextField(blank=True,max_length=300,verbose_name='metodología',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\-.,;:"#%&~^/\\(){}¡!¿?\s]{0,300}$',
            message = 'Solo se acepta la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][-_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 300 caracteres.'
    )]) # metodología del curso activo
    evaluate = models.TextField(blank=True,max_length=100,verbose_name='evaluación',
        validators = [RegexValidator(regex=r'^[\da-zA-ZáéíóúñÁÉÍÓÚÑ.+*\s]{0,100}$',
            message = 'Solo se acepta la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][.+*] y espacios, hasta 100 caracteres.'
    )]) # evaluación del curso activo
    silabus_dufa = models.FileField(null=True,blank=True,upload_to=silabus_dufa_path,verbose_name='silabo')
    # silabo dufa del curso activo
    status = models.BooleanField(blank=True,default=True,verbose_name='estado')
    # estado del curso activo
    created = models.DateTimeField(auto_now_add=True,verbose_name='fecha de creación')
    # fecha de creación de los datos (automático)
    updated = models.DateTimeField(auto_now=True,verbose_name='fecha de actualización')
    # fecha de actualización de los datos (automático)

    users = models.ManyToManyField(User,through='CourseActive_User',blank=True,related_name='courses_actives',verbose_name='responsables')
    # conjunto de id's de los responsables del curso activo
    student_results_descriptions = models.ManyToManyField(Course_StudentResult,through='CourseActive_StudentResult_Descriptions',blank=True,related_name='courses_actives',verbose_name='resultados del estudiante')
    # conjunto de id's de los resultados de estudiante del curso de malla (versión en ingles)
    competences_descriptions = models.ManyToManyField(Course_Competence,through='CourseActive_Competence_Descriptions',blank=True,related_name='courses_actives',verbose_name='competencias')
    # conjunto de id's de las competencias del curso de malla (versión en ingles)

    class Meta:
        verbose_name = 'curso activo del portafolio'
        verbose_name_plural = 'portafolios: cursos activos'
        unique_together = ['id_portfolio','id_course']

    def __str__(self): return f'[{self.id}] {self.id_course.name} | {self.id_portfolio}'

    def create_data_auto(self):
        '''Función para crear los registros por defecto de un curso activo'''
        EntranceExamination.objects.get_or_create(id_course_active=self)
        [[PeriodAcademic.objects.get_or_create(id_course_active=self,period=p[0],modality=m[0]) for m in PeriodAcademic.MODALITY] for p in PeriodAcademic.PERIOD[:3]]
        PeriodAcademic.objects.get_or_create(id_course_active=self,period=4,modality=3)
        [Stage.objects.get_or_create(id_course_active=self,stage=i) for i in range(1,5)]

    def evaluation_set(self): return Evaluation.objects.filter(id_evidence__id_period_academic__id_course_active=self)
    '''función para obtener los criterios evaluados del curso activo'''
    def silabus_dufa_filename(self): return get_filename(self.silabus_dufa)

class CourseActive_StudentResult_Descriptions(models.Model):
    '''Modelo de los cursos activos por resultados de estudiante en la base de datos'''
    id_course_active = models.ForeignKey(CourseActive,on_delete=models.CASCADE,null=False,blank=False,verbose_name='curso activo')
    # id del periodo académico de un curso activo
    id_course_student_result = models.ForeignKey(Course_StudentResult,on_delete=models.CASCADE,null=False,blank=False,verbose_name='resultado de estudiante')
    # id del recurso (curso por resultado de estudiante)
    description = models.TextField(blank=True,max_length=420,verbose_name='descripción',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,420}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 420 caracteres'
    )]) # descripción del resultado del estudiante (en ingles)

    class Meta:
        verbose_name = 'descripción en ingles de resultado del estudiante (recurso)'
        verbose_name_plural = 'descripción en ingles de resultados del estudiante (recurso)'
        unique_together = ['id_course_active','id_course_student_result']

    def __str__(self): return f'{self.id_course_student_result} ── {self.id_course_active}'

class CourseActive_Competence_Descriptions(models.Model):
    '''Modelo de los cursos activos por resultados de estudiante en la base de datos'''
    id_course_active = models.ForeignKey(CourseActive,on_delete=models.CASCADE,null=False,blank=False,verbose_name='curso activo')
    # id del periodo académico de un curso activo
    id_course_competence = models.ForeignKey(Course_Competence,on_delete=models.CASCADE,null=False,blank=False,verbose_name='competencia')
    # id del recurso (curso por competencia)
    description = models.TextField(blank=True,max_length=320,verbose_name='descripción',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,320}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 320 caracteres'
    )]) # descripción de la competencia (en ingles)

    class Meta:
        verbose_name = 'descripción en ingles de competencia (recurso)'
        verbose_name_plural = 'descripción en ingles de competencias (recurso)'
        unique_together = ['id_course_active','id_course_competence']

    def __str__(self): return f'{self.id_course_competence} ── {self.id_course_active}'

class Stage(models.Model):
    '''Modelo de las etapas de un curso activo en la base de datos'''
    id_course_active = models.ForeignKey(CourseActive,on_delete=models.CASCADE,null=False,blank=False,verbose_name='curso activo')
    # id del curso activo al que pertenece la etapa/fase
    stage = models.PositiveSmallIntegerField(blank=False,verbose_name='etapa',
        validators = [MinValueValidator(1),MaxValueValidator(4)]
    ) # etapa del curso activo
    observation = models.TextField(blank=True,max_length=100,verbose_name='observaciones',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,200}$',
            message = 'Solo se acepta la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 200 caracteres.'
    )]) # laboratorio del curso activo
    status = models.BooleanField(blank=True,default=False,verbose_name='estado')
    # estado del curso activo

    class Meta:
        verbose_name = 'estado de la etapa del curso activo'
        verbose_name_plural = 'estados de las etapas del curso activo'
        unique_together = ['id_course_active','stage']

    def __str__(self): return f'[{self.id}] etapa {self.stage} | {self.id_course_active}'

class CourseActive_User(models.Model):
    '''Modelo de los usuarios responsables de un curso activo en la base de datos'''
    id_course_active = models.ForeignKey(CourseActive,on_delete=models.CASCADE,null=False,blank=False,verbose_name='curso activo')
    # id del curso activo
    id_user = models.ForeignKey(User,on_delete=models.CASCADE,null=False,blank=False,verbose_name='responsable')
    # id del usuario responsable del curso activo
    group_thery = models.CharField(null=True,blank=True,max_length=40,verbose_name='grupos de teoría',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,:\s]{0,40}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:] y espacios, hasta 40 caracteres'
    )]) # grupos de teoría a cargo del responsable del curso activo
    group_laboratory = models.CharField(null=True,blank=True,max_length=40,verbose_name='grupos de laboratorio',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,:\s]{0,40}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:] y espacios, hasta 40 caracteres'
    )]) # grupos de laboratorio a cargo del responsable del curso activo
    level = models.TextField(blank=True,max_length=50,verbose_name='nivel',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\'\-.,:()"\s]{0,50}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][-_.,-:] y espacios, hasta 50 caracteres'
    )]) # niveles A, B, C del responsable

    class Meta:
        verbose_name = 'responsable del curso activo'
        verbose_name_plural = 'portafolios: cursos activos> responsables'
        unique_together = ['id_course_active', 'id_user']

    def __str__(self): return f'{self.id_user} ── {self.id_course_active}'

class FacultyVitaeAbet(models.Model):
    '''Modelo de las vitaes de facultad abet de un usuario en la base de datos'''
    id_user = models.OneToOneField(User,on_delete=models.CASCADE,null=False,blank=False,primary_key=True,verbose_name='usuario')
    # id del usuario responsable de un curso activo
    professional_title = models.CharField(blank=False,max_length=50,verbose_name='titulo profesional',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\'\-.,:()"\s]{12,50}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][\'-_.,:-()"] y espacios, de 12 a 50 caracteres'
    )]) # titulo profesional
    academic_degree = models.TextField(blank=True,max_length=100,verbose_name='grado académico',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\'\-.,:()"\s]{0,100}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][\'-_.,:-()"] y espacios, hasta 100 caracteres'
    )]) # grado académico del responsable
    education = models.TextField(blank=True,max_length=500,verbose_name='educación',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\'\-.,:()"\s]{0,500}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][\'-_.,:-()"] y espacios, hasta 500 caracteres'
    )]) # educación del responsable
    dina_register = models.TextField(blank=True,max_length=120,verbose_name='registro dina',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\'=+\-.,;:"#%&~^/\\(){}¡!¿?\s]{0,120}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][\'=+-_.,:;"#%&~^/\\(){}!¡¿?] y espacios, hasta 120 caracteres'
    )]) # url del registro dina del responsable
    experience_professional = models.TextField(blank=True,max_length=1500,verbose_name='experiencia profesional',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\'\-.,:()"\s]{0,1500}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][\'-_.,:-()"] y espacios, hasta 1500 caracteres'
    )]) # descripción de la experiencia profesional del responsable
    experience_academic = models.TextField(blank=True,max_length=1500,verbose_name='experiencia académica',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\'\-.,:()"\s]{0,1500}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][\'-_.,:-()"] y espacios, hasta 1500 caracteres'
    )]) # descripción de la experiencia académica del responsable
    collegue = models.TextField(blank=True,max_length=100,verbose_name='colegiatura',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\'=+\-.,;:"#%&~^/\\(){}¡!¿?\s]{0,100}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][\'=+-_.,:;"#%&~^/\\(){}!¡¿?] y espacios, hasta 100 caracteres'
    )]) # url del registro de colegiatura o grado académico del responsable
    societies = models.TextField(blank=True,max_length=200,verbose_name='sociedades',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\'\-.,:()"\s]{0,200}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][\'-_.,:-()"] y espacios, hasta 200 caracteres'
    )]) # sociedades a las que pertenece el responsable
    service = models.TextField(blank=True,max_length=300,verbose_name='servicios',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\'\-.,:()"\s]{0,300}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][\'-_.,:-()"] y espacios, hasta 300 caracteres'
    )]) # actividades de servicios del responsable
    awards = models.TextField(blank=True,max_length=500,verbose_name='premios',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\'\-.,:()"\s]{0,500}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][\'-_.,:-()"] y espacios, hasta 500 caracteres'
    )]) # premios y diplomas del responsable
    conferences = models.TextField(blank=True,max_length=2500,verbose_name='conferencias',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\'\-.,:()"\s]{0,2500}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][\'-_.,:-()"] y espacios, hasta 2500 caracteres'
    )]) # publicaciones y conferencias del responsable
    programs = models.TextField(blank=True,max_length=1000,verbose_name='programas',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\'\-.,:()"\s]{0,1000}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][\'-_.,:-()"] y espacios, hasta 1000 caracteres'
    )]) # programas de participación del responsable
    other = models.TextField(blank=True,max_length=1000,verbose_name='otros',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\'\-.,:()"\s]{0,1000}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][\'-_.,:-()"] y espacios, hasta 1000 caracteres'
    )]) # otras actividades profesionales del responsable
    idioms = models.TextField(blank=True,max_length=300,verbose_name='idiomas',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\'\-.,:()"\s]{0,300}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][\'-_.,:-()"] y espacios, hasta 300 caracteres'
    )]) # idiomas conocidos del responsable
    courses_dictates = models.TextField(blank=True,max_length=400,verbose_name='cursos dictados',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ\'\-.,:()"\s]{0,400}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][\'-_.,:-()"] y espacios, hasta 400 caracteres'
    )]) # cursos dictados del responsable

    class Meta:
        verbose_name = 'vitae de facultad abet del usuario'
        verbose_name_plural = 'vitaes de facultad abet del usuario'

    def __str__(self): return f'Vitae de Facultad > {self.id_user}'

class Material(models.Model):
    '''Modelo del materiales del curso activo en la base de datos'''
    id_course_active = models.ForeignKey(CourseActive,on_delete=models.CASCADE,null=False,blank=False,verbose_name='curso activo')
    # id del curso activo al que pertenece el material
    title = models.CharField(blank=False,max_length=100,verbose_name='titulo',
        validators = [RegexValidator(regex=r'^[\wa-záéíóúñÁÉÍÓÚÑ.,:\s]{5,100}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:] y espacios, de 5 a 100 caracteres'
    )]) # titulo del material
    author = models.CharField(blank=True,max_length=30,verbose_name='autor',
        validators = [RegexValidator(regex=r'^[\wa-záéíóúñÁÉÍÓÚÑ.,:\s]{2,30}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:] y espacios, de 2 a 30 caracteres'
    )]) # autor del material
    url = models.URLField(null=True,blank=True,max_length=500, verbose_name='dirección url',
        validators = [RegexValidator(regex=r'^[\wa-záéíóúñÁÉÍÓÚÑ\'=+\-.,;:"#%&~^/\\(){}¡!¿?\s]{10,500}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][\'=+-_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, de 10 a 500 caracteres'
    )]) # dirección url del material
    year = models.PositiveSmallIntegerField(null=True,blank=True,verbose_name='año de publicación',
        validators = [MinValueValidator(2000),MaxValueValidator(2050)]
    ) # año de publicación del material
    other = models.BooleanField(blank=True,default=False,verbose_name='otro formato')
    # estado para verificar si un material no es ieee

    class Meta:
        verbose_name = 'material del curso activo'
        verbose_name_plural = 'materiales del curso activo'
        unique_together = ['id_course_active', 'title']

    def __str__(self): return f'[{self.id}] {self.title} | {self.id_course_active}'

# PERIODO ACADEMICO ───────────────────────────────────────────────────────────

class PeriodAcademic(models.Model):
    '''Modelo de periodos académicos del curso activo en la base de datos'''
    PERIOD = ((1,'primer periodo'),(2,'segundo periodo'),(3,'tercer periodo'),(4,'final'))
    MODALITY = ((1,'examen'),(2,'continua'),(3,'general'))

    id_course_active = models.ForeignKey(CourseActive,on_delete=models.CASCADE,null=False,blank=False,verbose_name='curso activo')
    # id del curso activo al que pertenece el periodo académico
    period = models.PositiveSmallIntegerField(blank=False,default=1,choices=PERIOD,verbose_name='periodo',
        validators = [MinValueValidator(1), MaxValueValidator(4)]
    ) # periodo académico
    modality = models.PositiveSmallIntegerField(blank=False,default=1,choices=MODALITY,verbose_name='modalidad',
        validators = [MinValueValidator(1), MaxValueValidator(3)]
    ) # modalidad del periodo académico

    student_results = models.ManyToManyField(Course_StudentResult,through='PeriodAcademic_StudentResult',blank=True,related_name='periods_academics',verbose_name='resultados del estudiante')
    # conjunto de id's de los resultados de estudiante del curso activo (por periodos)
    competences = models.ManyToManyField(Course_Competence,through='PeriodAcademic_Competence',blank=True,related_name='periods_academics',verbose_name='competencias')
    # conjunto de id's de las competencias del curso activo (por periodos)

    class Meta:
        verbose_name = 'periodo académico del curso activo'
        verbose_name_plural = 'portafolios: cursos activos> periodos académicos'
        unique_together = ['id_course_active', 'period', 'modality']

    def __str__(self): return f'{self.get_period_display()} - {self.get_modality_display()} | {self.id_course_active}'

    def period_complete(self): return f'{self.get_period_display()} - {self.get_modality_display()}'
    period_complete.short_description = 'periodo académico'

class PeriodAcademic_StudentResult(models.Model):
    '''Modelo de los cursos activos por resultados de estudiante en la base de datos'''
    METHODS = ((0,'rubrica'), (1,'lista de cotejos'), (2,'otros'),)

    id_period_academic = models.ForeignKey(PeriodAcademic,on_delete=models.CASCADE,null=False,blank=False,verbose_name='periodo académico')
    # id del periodo académico de un curso activo
    id_course_student_result = models.ForeignKey(Course_StudentResult,on_delete=models.CASCADE,null=False,blank=False,verbose_name='resultado de estudiante')
    # id del recurso (curso por resultado de estudiante)
    description = models.TextField(blank=True,max_length=420,verbose_name='descripción',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,420}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 420 caracteres'
    )]) # descripción del resultado del estudiante del curso activo
    method = models.PositiveSmallIntegerField(blank=True,default=0,choices=METHODS,verbose_name='método',
        validators = [MaxValueValidator(2)]
    ) # método de medición del resultado de estudiante del curso activo
    result = models.PositiveSmallIntegerField(blank=True,default=0,verbose_name='resultado de evaluación',
        validators = [MaxValueValidator(100)]
    ) # resultado de evaluación del resultado de estudiante del curso activo

    class Meta:
        verbose_name = 'resultado del estudiante del curso activo (por periodo)'
        verbose_name_plural = 'resultados del estudiante del curso activo (por periodo)'
        unique_together = ['id_period_academic','id_course_student_result']

    def __str__(self): return f'{self.id_course_student_result} ── {self.id_period_academic}'

class PeriodAcademic_Competence(models.Model):
    '''Modelo de los cursos activos por resultados de estudiante en la base de datos'''
    id_period_academic = models.ForeignKey(PeriodAcademic,on_delete=models.CASCADE,null=False,blank=False,verbose_name='periodo académico')
    # id del periodo académico de un curso activo
    id_course_competence = models.ForeignKey(Course_Competence,on_delete=models.CASCADE,null=False,blank=False,verbose_name='competencia')
    # id del recurso (curso por competencia)
    description = models.TextField(blank=True,max_length=320,verbose_name='descripción',
        validators = [RegexValidator(regex=r'^[\wáéíóúñÁÉÍÓÚÑ.,;:"#%&~^/\\(){}¡!¿?\s]{0,320}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, hasta 320 caracteres'
    )]) # descripción de la competencia del curso activo

    class Meta:
        verbose_name = 'competencia del curso activo (por periodo)'
        verbose_name_plural = 'competencias del curso activo (por periodo)'
        unique_together = ['id_period_academic','id_course_competence']

    def __str__(self): return f'{self.id_course_competence} ── {self.id_period_academic}'

class EntranceExamination(models.Model):
    '''Modelo de exámenes de entrada del curso activo en la base de datos'''
    id_course_active = models.OneToOneField(CourseActive,on_delete=models.CASCADE,null=False,blank=False,primary_key=True,verbose_name='curso activo')
    # id del curso activo al que pertenece el examen de entrada
    knowledges = ArrayField(models.CharField(max_length=50,
        validators = [RegexValidator(regex=r'^[\da-zA-ZáéíóúñÁÉÍÓÚÑ\s]{5,50}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ] y espacios, de 5 a 50 caracteres'
    )]),null=False,blank=True,size=6,default=list,verbose_name='conocimientos')
    # conocimientos/habilidades para el examen de entrada
    doc_exam = models.FileField(null=True,blank=True,upload_to=EntranceExamination_path,verbose_name='examen')
    # documento del examen de el examen de entrada
    doc_exam_solution = models.FileField(null=True,blank=True,upload_to=EntranceExamination_path,verbose_name='solución de examen')
    # documento de solución del examen de entrada

    class Meta:
        verbose_name = 'evaluación de entrada del curso activo'
        verbose_name_plural = 'evaluaciones de entrada del curso activo'

    def __str__(self): return f'evaluación de entrada > {self.id_course_active}'
    def doc_exam_filename(self): return get_filename(self.doc_exam)
    def doc_exam_solution_filename(self): return get_filename(self.doc_exam_solution)

class Evidence(models.Model):
    '''Modelo de evidencias de periodos académicos en la base de datos'''
    id_period_academic = models.ForeignKey(PeriodAcademic,on_delete=models.CASCADE,null=False,blank=False,verbose_name='periodo académico')
    # id del periodo académico al que pertenece la evidencia
    description = models.CharField(blank=False,max_length=100,verbose_name='descripción',
        validators = [RegexValidator(regex=r'^[\dwa-záéíóúñÁÉÍÓÚÑ_.,;:"#%&~^/\\(){}¡!¿?\s]{5,100}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\(){}!¡¿?] y espacios, de 5 a 100 caracteres'
    )]) # descripción de la evidencia

    class Meta:
        verbose_name = 'evidencia de curso activo (por periodo)'
        verbose_name_plural = 'portafolios: cursos activos> periodos académicos> evidencias'

    def __str__(self): return f'[{self.id}] evidencia | {self.id_period_academic}'

class EvidenceDocument(models.Model):
    '''Modelo de documento de evidencias de periodos académicos en la base de datos'''
    id_period_academic = models.ForeignKey(PeriodAcademic,on_delete=models.CASCADE,null=False,blank=False,verbose_name='periodo académico')
    # id del periodo académico al que pertenece el documento de evidencia
    doc_annexed = models.FileField(null=False,blank=False,upload_to=EvidenceDocument_path,verbose_name='documento anexo',
        validators = [FileExtensionValidator(allowed_extensions=['docx','pptx','xlsx','pdf','odp'],
            message = 'Solo se aceptan los siguientes formatos: docx,pptx,xlsx,pdf,odp'
    )]) # documento anexo de evidencia

    class Meta:
        verbose_name = 'documento de evidencia del curso activo (por periodo)'
        verbose_name_plural = 'documentos de evidencia del curso activo (por periodo)'

    def __str__(self): return f'[{self.id}] documento evidencia | {self.id_period_academic}'
    def doc_annexed_filename(self): return get_filename(self.doc_annexed)

class Evaluation(models.Model):
    '''Modelo de evaluaciones (de criterios) de una evidencia de periodo académico en la base de datos'''
    id_evidence = models.ForeignKey(Evidence,on_delete=models.CASCADE,null=False,blank=False,verbose_name='evidencia')
    # id de la evidencia del periodo académico que tiene esta evaluación
    id_criteria = models.ForeignKey(Criteria,on_delete=models.CASCADE,null=True,blank=True,verbose_name='criterio')
    # id del criterio del resultado de estudiante del curso activo
    aspect = models.CharField(blank=False,max_length=100,verbose_name='aspecto',
        validators = [RegexValidator(regex=r'^[\wa-záéíóúñÁÉÍÓÚÑ_.,;:"#%&~^/\\(){}¡!¿?\s]{30,100}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\(){}!¡¿?] y espacios, de 30 a 100 caracteres'
    )]) # aspectos de la evaluación
    criteria = models.TextField(null=True,blank=True,max_length=256,verbose_name='criterio',
        validators = [RegexValidator(regex=r'^[\wa-záéíóúñÁÉÍÓÚÑ_.,;:"#%&~^/\\(){}¡!¿?\s]{0,256}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\(){}!¡¿?] y espacios, hasta 256 caracteres'
    )]) # descripción del criterio en la evaluación
    level1 = models.TextField(null=True,blank=True,max_length=156,verbose_name='nivel 1',
        validators = [RegexValidator(regex=r'^[\wa-záéíóúñÁÉÍÓÚÑ_.,;:"#%&~^/\\(){}¡!¿?\s]{0,156}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\(){}!¡¿?] y espacios, hasta 156 caracteres'
    )]) # descripción del nivel 1 (insatisfactorio 25%) de la evaluación
    level2 = models.TextField(null=True,blank=True,max_length=156,verbose_name='nivel 2',
        validators = [RegexValidator(regex=r'^[\wa-záéíóúñÁÉÍÓÚÑ_.,;:"#%&~^/\\(){}¡!¿?\s]{0,156}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\(){}!¡¿?] y espacios, hasta 156 caracteres'
    )]) # descripción del nivel 2 (en proceso 50%) de la evaluación
    level3 = models.TextField(null=True,blank=True,max_length=156,verbose_name='nivel 3',
        validators = [RegexValidator(regex=r'^[\wa-záéíóúñÁÉÍÓÚÑ_.,;:"#%&~^/\\(){}¡!¿?\s]{0,156}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\(){}!¡¿?] y espacios, hasta 156 caracteres'
    )]) # descripción del nivel 3 (satisfactorio 75%) de la evaluación
    level4 = models.TextField(null=True,blank=True,max_length=156,verbose_name='nivel 4',
        validators = [RegexValidator(regex=r'^[\wa-záéíóúñÁÉÍÓÚÑ_.,;:"#%&~^/\\(){}¡!¿?\s]{0,156}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\(){}!¡¿?] y espacios, hasta 156 caracteres'
    )]) # descripción del nivel 4 (sobresaliente 100%) de la evaluación
    review_week = models.PositiveSmallIntegerField(null=True,blank=True,verbose_name='semana de revisión',
        validators = [MinValueValidator(1),MaxValueValidator(17)]
    ) # semana de evaluación del critreio de resultado de estudiante

    class Meta:
        verbose_name = 'portafolio: curso activo> periodo académico> evaluación de evidencia'
        verbose_name_plural = 'portafolios: cursos activos> periodos académicos> evaluaciones de evidencias'
        unique_together = ['id_evidence','id_criteria']

    def __str__(self): return f'[{self.id}] evaluación | {self.id_evidence}'

    def get_criteria_name(self): return self.id_criteria.name if self.id_criteria else self.id_criteria
    '''Función para obtener el nombre del criterio'''
    def get_studentresult_name(self): return self.id_criteria.id_student_result.name if self.id_criteria else self.id_criteria
    '''Función para obtener el nombre del resultado de estudiante'''
    def get_studentresult_method(self):
        '''Función para obtener el metodo del resultado de estudiante'''
        if not self.id_criteria: return self.id_criteria
        casr = PeriodAcademic_StudentResult.objects.filter(
            id_period_academic=self.id_evidence.id_period_academic,
            id_course_student_result__id_student_result=self.id_criteria.id_student_result)
        return casr[0].get_method_display() if casr.exists() else None

# DESARROLLO ACADEMICO ────────────────────────────────────────────────────────

class AcademicDevelopment(models.Model):
    '''Modelo del desarrollo académico del curso activo en la base de datos'''
    id_course_active = models.ForeignKey(CourseActive,on_delete=models.CASCADE,null=False,blank=False,verbose_name='curso activo')
    # id del curso activo al que pertenece el desarrollo académico
    activity = models.CharField(blank=False,max_length=200,verbose_name='actividad',
        validators = [RegexValidator(regex=r'^[\da-zA-ZáéíóúñÁÉÍÓÚÑ\s]{5,200}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ] y espacios, de 5 a 200 caracteres'
    )]) # actividad en el desarrollo  académico del curso activo
    theme = models.CharField(blank=False,max_length=200,verbose_name='tema',
        validators = [RegexValidator(regex=r'^[\da-zA-ZáéíóúñÁÉÍÓÚÑ\s]{0,200}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ] y espacios, hasta 200 caracteres'
    )]) # tema en el desarrollo académico del curso activo

    class Meta:
        verbose_name = 'desarrollo académico del curso activo'
        verbose_name_plural = 'desarrollos académicos del curso activo'

    def __str__(self): return f'[{self.id}] desarrollo académico | {self.id_course_active}'

class Student(models.Model):
    '''Modelo de los estudiantes de un curso activo en la base de datos'''
    id_course_active = models.ForeignKey(CourseActive,on_delete=models.CASCADE,null=False,blank=False,verbose_name='curso activo')
    # id del curso activo al que pertenece el estudiante
    cui = models.PositiveIntegerField(blank=False,verbose_name='cui',
        validators = [MinValueValidator(20000000), MaxValueValidator(99999999)]
    ) # código del alumno
    full_name = models.CharField(blank=False,max_length=100,verbose_name='nombre completo',
        validators = [RegexValidator(
            regex = r'^[a-záéíóúñ\s]{8,100}$',
            message = 'Solo se admite la siguiente validación: [a-z][á-úñ] y espacios, de 8 a 100 caracteres'
    )]) # nombre completo del estudiante
    status = models.BooleanField(blank=True,default=True,verbose_name='estado')
    # estado del estudiante
    grades_entrance = ArrayField(models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(20)]
    ),null=True,blank=True,default=list,verbose_name='calificaciones de examen de entrada')
    # conjunto de calificaciones del examen de entrada

    grades_criteria = models.ManyToManyField(Evaluation,through='GradeCriteria',blank=True,related_name='students',verbose_name='calificaciones por criterios')
    # conjunto de id's de las calificaciones por resultados del estudiante
    grades_period = models.ManyToManyField(PeriodAcademic,through='GradePeriod',blank=True,related_name='students',verbose_name='calificaciones por periodos académicos')
    # conjunto de id's de las calificaciones por periodos académicos del estudiante

    class Meta:
        verbose_name = 'estudiante del curso activo'
        verbose_name_plural = 'portafolios: cursos activos> estudiantes'
        unique_together = ['id_course_active', 'cui']

    def __str__(self): return f'[{self.id}] {self.full_name} [{self.cui}] | {self.id_course_active}'

class GradeCriteria(models.Model):
    '''Modelo de las calificaciones por criterio de un estudiante en la base de datos'''
    id_student = models.ForeignKey(Student,on_delete=models.CASCADE,null=False,blank=False,verbose_name='estudiante')
    # id del estudiante
    id_evaluation = models.ForeignKey(Evaluation,on_delete=models.CASCADE,null=False,blank=False,verbose_name='criterio')
    # id del criterio que se esta evaluando
    grade = models.PositiveSmallIntegerField(null=True,blank=False,verbose_name='calificación',
        validators=[MaxValueValidator(20)]
    ) # calificación por criterio del estudiante

    class Meta:
        verbose_name = 'calificación por criterio del estudiante'
        verbose_name_plural = 'calificaciones por criterio del estudiante'
        unique_together = ['id_student', 'id_evaluation']

    def __str__(self): return f'[{self.id}] {self.id_student.full_name} ── {self.id_evaluation}: {self.grade}'

class GradePeriod(models.Model):
    '''modelo de las calificaciones por periodo académico de un estudiante en la base de datos'''
    id_student = models.ForeignKey(Student,on_delete=models.CASCADE,null=False,blank=False,verbose_name='estudiante')
    # id del estudiante al que pertenece la calificación
    id_period_academic = models.ForeignKey(PeriodAcademic,on_delete=models.CASCADE,null=False,blank=False,verbose_name='periodo académico')
    # id del periodo académico
    grade = models.PositiveSmallIntegerField(null=True,blank=False,verbose_name='calificación',
        validators = [MaxValueValidator(20)]
    ) # calificación por periodo académico del estudiante
   
    class Meta:
        verbose_name = 'calificación por periodos del estudiante'
        verbose_name_plural = 'calificaciones por periodos del estudiante'
        unique_together = ['id_student', 'id_period_academic']

    def __str__(self): return f'[{self.id}] {self.id_student.full_name} ── {self.id_period_academic}: {self.grade}'

class Incidence(models.Model):
    '''Modelo de los incidentes de un estudiante en la base de datos'''
    id_student = models.ForeignKey(Student,on_delete=models.CASCADE,null=False,blank=False,verbose_name='estudiante')
    # id del estudiante involucrado en la incidencia
    incidence = models.TextField(blank=False,max_length=100,verbose_name='incidencia',
        validators = [RegexValidator(regex=r'^[\wa-záéíóúñÁÉÍÓÚÑ_.,;:"#%&~^/\\(){}¡!¿?\s]{5,100}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, de 5 a 100 caracteres'
    )]) # descripción de la incidencia
    solution = models.TextField(blank=False,max_length=100,verbose_name='solución',
        validators = [RegexValidator(regex=r'^[\wa-záéíóúñÁÉÍÓÚÑ_.,;:"#%&~^/\\(){}¡!¿?\s]{5,100}$',
            message = 'Solo se admite la siguiente validatorslidación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\()/{}!¡¿?] y espacios, de 5 a 100 caracteres'
    )]) #  solución de la incidencia
    quantity = models.PositiveSmallIntegerField(blank=False,verbose_name='cantidad de involucrados',
        validators = [MinValueValidator(1), MaxValueValidator(40)]
    ) # cantidad de involucrados en la incidencia
    date = models.DateTimeField(null=False,blank=False,verbose_name='fecha de incidencia')
    # fecha de la incidencia
    evidence = models.FileField(null=True,blank=True,upload_to=incidence_path,verbose_name='evidencia')
    # documento de evidencia de la incidencia

    class Meta:
        verbose_name = 'incidencia del estudiante'
        verbose_name_plural = 'incidencias del estudiante'

    def __str__(self): return f'[{self.id}] incidencia | {self.id_student}'
    def evidence_filename(self): return get_filename(self.evidence)

class ContinuousImprovement(models.Model):
    '''Modelo de la mejora continua del curso activo en la base de datos'''
    id_course_active = models.ForeignKey(CourseActive,on_delete=models.CASCADE,null=False,blank=False,verbose_name='curso activo')
    # id del curso activo al que pertenece la mejora continua
    deficiency = models.CharField(blank=False,max_length=50,verbose_name='deficiencia',
        validators = [RegexValidator(regex=r'^[\wa-záéíóúñÁÉÍÓÚÑ_.,;:"#%&~^/\\(){}¡!¿?\s]{5,50}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\(){}!¡¿?] y espacios, de 5 a 50 caracteres'
    )]) # Descripción de la deficiencia de la mejora continua
    improvement = models.CharField(blank=False,max_length=50,verbose_name='mejora',
        validators = [RegexValidator(regex=r'^[\wa-záéíóúñÁÉÍÓÚÑ_.,;:"#%&~^/\\(){}¡!¿?\s]{5,50}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\(){}!¡¿?] y espacios, de 5 a 50 caracteres'
    )]) # Descripción de la mejora a realizar en la mejora continua
    result = models.CharField(blank=False,max_length=50,verbose_name='resultado',
        validators = [RegexValidator(regex=r'^[\wa-záéíóúñÁÉÍÓÚÑ_.,;:"#%&~^/\\(){}¡!¿?\s]{5,50}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\(){}!¡¿?] y espacios, de 5 a 50 caracteres'
    )]) # Descripción de los resultados de la mejora continua
    class Meta:
        verbose_name = 'mejora continua del curso activo'
        verbose_name_plural = 'mejoras continuas del curso activo'

    def __str__(self): return f'[{self.id}] mejora continua | {self.id_course_active}'

class ProposalImprovement(models.Model):
    '''Modelo de la propuesta de mejora del curso activo en la base de datos'''
    id_course_active = models.ForeignKey(CourseActive,on_delete=models.CASCADE,null=False,blank=False,verbose_name='curso activo')
    # id del curso activo al que pertenece la propuesta de la mejora
    scope = models.CharField(blank=False,max_length=50,verbose_name='alcance',
        validators = [RegexValidator(regex=r'^[\wa-záéíóúñÁÉÍÓÚÑ_.,;:"#%&~^/\\(){}¡!¿?\s]{5,50}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\(){}!¡¿?] y espacios, de 5 a 50 caracteres'
    )]) # descripción del alcance de la propuesta de mejora
    proposal = models.CharField(blank=False,max_length=50,verbose_name='propuesta',
        validators = [RegexValidator(regex=r'^[\wa-záéíóúñÁÉÍÓÚÑ_.,;:"#%&~^/\\(){}¡!¿?\s]{5,50}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\(){}!¡¿?] y espacios, de 5 a 50 caracteres'
    )]) # descripción de la propuesta de la mejora
    expected_results = models.CharField(blank=True,max_length=50,verbose_name='resultados previstos',
        validators = [RegexValidator(regex=r'^[\wa-záéíóúñÁÉÍÓÚÑ_.,;:"#%&~^/\\(){}¡!¿?\s]{5,50}$',
            message = 'Solo se admite la siguiente validación: [aA-zZ][0-9][áÁ-úÚñÑ][_.,:;"#%&~^/\\(){}!¡¿?] y espacios, de 5 a 50 caracteres'
    )]) # descripción de los resultados de la propuesta de mejora

    class Meta:
        verbose_name = 'propuesta de mejora continua del curso activo'
        verbose_name_plural = 'propuestas de mejora continua del curso activo'

    def __str__(self): return f'[{self.id}] propuesta de mejora | {self.id_course_active}'

# REGISTROS AUTOMATICOS ═══════════════════════════════════════════════════════════════════════════

from django.dispatch import receiver

@receiver(models.signals.post_save, sender=FormationArea)
def create_data_formation_area(sender, instance, created, **kwargs):
    if created: Subarea.objects.get_or_create(id_formation_area=instance, name='general')

@receiver(models.signals.m2m_changed, sender=Portfolio.courses.through)
def create_data_portfolio(sender, **kwargs):
    if kwargs['action']=='post_add': [cource_active.create_data_auto() for cource_active in kwargs['instance'].courseactive_set.all()]

@receiver(models.signals.post_save, sender=CourseActive)
def create_data_course(sender, instance, created, **kwargs):
    if created: instance.create_data_auto()