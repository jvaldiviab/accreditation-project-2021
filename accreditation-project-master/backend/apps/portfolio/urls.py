from apps.portfolio.api import *
from django.urls import path, include
from rest_framework import routers

router = routers.SimpleRouter()

router.register('formation_area', FormationArea_API)
router.register('subarea', Subarea_API)

router.register('resource', Resource_API)

router.register('student_result', StudentResult_API)
router.register('criteria', Criteria_API)

router.register('competence', Competence_API)

router.register('course', Course_API)
router.register('course_student_results', Course_StudentResult_API)
router.register('course_competence', Course_Competence_API)

router.register('portfolio', Portfolio_API)
router.register('course_active', CourseActive_API)
router.register('course_active_user', CourseActive_User_API)
router.register('faculty_vitae_abet', FacultyVitaeAbet_API)
router.register('material', Material_API)

router.register('period_academic', PeriodAcademic_API)
router.register('entrance_examination', EntranceExamination_API)
router.register('evidence', Evidence_API)
router.register('evidence_document', EvidenceDocument_API)
router.register('evaluation', Evaluation_API)

router.register('academic_development',AcademicDevelopment_API)
router.register('student', Student_API)
router.register('incidence', Incidence_API)
router.register('continuous_improvement', ContinuousImprovement_API)
router.register('proposal_improvement', ProposalImprovement_API)

urlpatterns = router.urls
urlpatterns += [
    path('department/', Department_API.as_view(), name='api_department'),
    path('template/', Template_API.as_view(), name='api_template'),

    path('period_academic_student_results/<int:pk>/', PeriodAcademic_StudentResult_API.as_view(), name='api_period_academic_results_student'),
    path('period_academic_competences/<int:pk>/', PeriodAcademic_Competence_API.as_view(), name='api_period_academic_competence'),

    path('course_active_stages/<int:pk>/', CourseActive_Stage_API.as_view(), name='course_active_stages'),
    path('course_active_students/<int:pk>/', CourseActive_Student_API.as_view(), name='course_active_students'),
    path('course_active_review_plan/<int:pk>/', CourseActive_ReviewPlan_API.as_view(), name='course_active_review_plan'),
    path('course_active_silabus_abet/<int:pk>/', CourseActive_SilabusAbet_API.as_view(), name='course_active_silabus_abet'),
]