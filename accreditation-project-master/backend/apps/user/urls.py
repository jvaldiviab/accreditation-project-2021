from apps.user.api import *
from django.urls import path, include
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token, refresh_jwt_token

router = routers.SimpleRouter()
router.register('user', UserAPI)
router.register('role', RoleAPI)

urlpatterns = router.urls
urlpatterns += [
    path('obtain_token/', obtain_jwt_token, name='api_obtain_token'),
    path('verify_token/', verify_jwt_token, name='api_verify_token'),
    path('refresh_token/', refresh_jwt_token, name='api_refresh_token'),
    path('active/', UserActiveAPI.as_view(), name='api_user_active'),
    path('request-reset-email', User_Request_ResetPassword_API.as_view(), name='request-reset-email'),
    path('password-reset-complete', User_ResetPassword_API.as_view(), name='password-reset-complete'),
    path('password-reset/<uidb64>/<token>/', User_VerifyToken_ResetPassword_API.as_view(), name='password-reset-confirm'),
]