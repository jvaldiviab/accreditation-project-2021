from accreditation import settings
from apps.user.models import *
from apps.user.serializers import *
from django.conf import Settings
from django.core.mail.message import EmailMultiAlternatives
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework import viewsets, generics, status
from rest_framework.decorators import permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.permissions import BasePermission, IsAuthenticated, IsAdminUser, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.settings import api_settings

# ROLES ──────────────────────────────────────────────────

class RoleAPI(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de roles
    ENTRADA: id_process, name, permissions[{permission, module}]
    SALIDA: id, id_process, name, permissions[{permission, module}]
    PERMISSION:
    - 0:crear\n- 1:visualizar\n- 2:editar\n- 3:eliminar\n- 4:desactivar
    MODULE:
    - 0:roles\n- 1:usuarios\n- 2:fases\n- 3:portafolio
    '''
    queryset = Role.objects.all()
    serializer_class = RoleSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['id_process']

# USUARIOS ──────────────────────────────────────────────────

class UserAPI(viewsets.ModelViewSet):
    '''
    API general para todo el CRUD de usuarios
    ENTRADA: id_role, email, first_name, last_name, photo, signature, status
    SALIDA: id, id_role, email, first_name, last_name, photo, signature, superadmin, status
    '''
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = [DjangoFilterBackend,filters.SearchFilter,filters.OrderingFilter]
    filter_fields = ['id_role__id_process']
    search_fields = ['email']
    ordering_fields = ['first_name','last_name']

class UserActiveAPI(generics.RetrieveUpdateAPIView):
    '''
    Api para VER y EDITAR al usuario con session activa
    ENTRADA: id_role, email, first_name, last_name, photo, signature, status
    SALIDA: id, email, first_name, last_name, photo, signature, superadmin, status, role{}
    PERMISSION:
    - 0:crear\n- 1:visualizar\n- 2:editar\n- 3:eliminar\n- 4:desactivar
    MODULE:
    - 0:roles\n- 1:usuarios\n- 2:fases\n- 3:portafolio
    '''
    serializer_class = User_Active_Serializer

    def retrieve(self, request, *args, **kwargs):
        serializer = self.get_serializer(request.user)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        serializer = self.get_serializer(request.user, data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data, status=status.HTTP_200_OK)

class User_Request_ResetPassword_API(generics.GenericAPIView):
    '''
    API para enviar un link para restablecer la contraseña al correo electrónico de un usuario
    ENTRADA: email
    SALIDA: success
    '''
    serializer_class = User_ResetPassword_Serializer
    permission_classes = [AllowAny]

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if encode_user_password_reset(request.data['email']): return Response({'success': True},status=status.HTTP_200_OK)
        else: return Response({'success': False},status=status.HTTP_400_BAD_REQUEST)

class User_VerifyToken_ResetPassword_API(generics.GenericAPIView):
    '''
    API para verificar las credenciales de un usuario que quiere restablecer su contraseña
    SALIDA: success
    ''' 
    permission_classes = [AllowAny]

    def get(self, request, uidb64, token):
        if decode_user_password_reset(uidb64,token): return Response({'success': True},status=status.HTTP_200_OK)
        else: return Response({'success': False},status=status.HTTP_400_BAD_REQUEST)

class User_ResetPassword_API(generics.GenericAPIView):
    '''
    API para cambia la contraseña de un usuario
    ENTRADA: password, uidb64, token
    ''' 
    serializer_class = User_ResetPassword_Serializer
    permission_classes = [AllowAny]

    def patch(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = decode_user_password_reset(request.data['uidb64'],request.data['token'])
        print(user)
        if user:
            user.set_password(request.data['password'])
            user.save()
            return Response({'success': True},status=status.HTTP_200_OK)
        else: return Response({'success': False},status=status.HTTP_400_BAD_REQUEST)