from apps.user.models import *
from rest_framework import serializers
from accreditation.utils import *
from django.utils.crypto import get_random_string

# ROLES ──────────────────────────────────────────────────

class PermissionSerializer(serializers.ModelSerializer):
    '''Serializador general de los datos de permisos'''
    class Meta:
        model  = Permission
        fields = ['permission','module']

class RoleSerializer(serializers.ModelSerializer):
    '''Serializador general de los datos de roles'''
    permissions = PermissionSerializer(source='permission_set',many=True,required=False,label='permisos')

    class Meta:
        model  = Role
        fields = ['id','id_process','name','permissions']

    def __init__(self, *args, **kwargs):
        '''Función inicial cada vez que la clase es llamada'''
        super().__init__(*args, **kwargs)
        # si el método invocado es POST se deshabilitan algunos campos
        method = self.context['request'].method
        remove_fields(method=='POST', self.fields, 'status') #<------------------ POR DEFINIRSE
        # se verifica si el usuario logueado es super administrador
        self.user = self.context["request"].user
        if self.user.superadmin: return
        # se valida el proceso del usuario logueado
        remove_fields(method in ('POST','PUT'), self.fields, 'id_process')

    def get_or_create_permissions(self, instance, **fields): return Permission.objects.get_or_create(id_role=instance, **fields)[0]
    '''Función para crear y/o obtener permisos para el rol'''
    def create(self, validated_data):
        '''Función para crear un registro del modelo'''
        # si el usuario logueado no es super administrador se define el proceso al que pertenece
        if not self.user.superadmin: validated_data['id_process'] = self.user.id_role.id_process
        return create_nested(self, validated_data, 'permission_set', self.get_or_create_permissions)
    def update(self, instance, validated_data): return update_nested(self, instance, validated_data, 'permission_set', self.get_or_create_permissions)
    '''Función para actualizar un registro del modelo'''

class RoleActiveSerializer(serializers.ModelSerializer):
    '''Serializador de los datos del role de un usuario con sesión activa'''
    process = serializers.CharField(source='id_process.name', read_only=True)
    permissions = PermissionSerializer(source='permission_set', many=True, read_only=True)

    class Meta:
        model  = Role
        fields = ['name','process','id_process','permissions']

# USUARIOS ──────────────────────────────────────────────────

class UserSerializer(serializers.ModelSerializer):
    '''Serializador general de los datos de usuario'''
    class Meta:
        model = User
        exclude = ['password','superadmin','last_login','created','updated']

    def __init__(self, *args, **kwargs):
        '''Función inicial cada vez que la clase es llamada'''
        super().__init__(*args, **kwargs)
        # si el método invocado es POST se deshabilitan algunos campos
        method = self.context['request'].method
        remove_fields(method=='POST', self.fields, 'photo','signature','status')
        # se verifica si el usuario logueado es super administrador
        user = self.context["request"].user
        if user.superadmin: return
        # se valida el proceso del usuario logueado
        process = user.id_role.id_process
        self.fields['id_role'].queryset = Role.objects.filter(id_process=process)
        self.fields['id_role'].allow_null = False

    def create(self, validated_data):
        instance = super().create(validated_data)
        password_default = get_random_string(length=8)
        instance.set_password(password_default)
        instance.save()
        User.send_email(
            'Registro en el Sistema de Acreditación', #titulo del mensaje
            f"Hola {validated_data['first_name']}<br>\
            Su contraseña para el Sistema de Acreditación de la UNSA es:<br>\
            <strong>{password_default}</strong>", #cuerpo del mensaje
            validated_data['email'] #email de destino
        )
        return instance

class User_Active_Serializer(serializers.ModelSerializer):
    '''Serializador de los datos del usuario con sesión activa'''
    class Meta:
        model  = User
        exclude = ['password','id_role','last_login','created','updated']

    def __init__(self, *args, **kwargs):
        '''Función inicial cada vez que la clase es llamada'''
        super().__init__(*args, **kwargs)
        # si el método invocado es GET se crean algunos campos
        method = self.context['request'].method
        if method=='GET': self.fields['role'] = RoleActiveSerializer(read_only=True, source='id_role')
        # si el método invocado no es GET o no es superadmin se deshabilita el campo superadmin
        user = self.context["request"].user
        remove_fields(not method=='GET' or not user.superadmin, self.fields, 'superadmin')
        # si el método invocado es POST,PUT,PATH y es superadmin se deshabilita el estado
        remove_fields(method in ('POST','PUT','PATH') and user.superadmin, self.fields, 'status')

class User_ResetPassword_Serializer(serializers.ModelSerializer):
    '''Serializador del email de un usuario'''
    email    = serializers.EmailField(label='Correo electrónico')
    uidb64   = serializers.CharField()
    token    = serializers.CharField()

    class Meta:
        model = User
        fields = ['email','password','uidb64','token']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        method = self.context['request'].method
        remove_fields(method=='POST', self.fields, 'password','token','uidb64')
        remove_fields(method=='PATCH', self.fields, 'email')