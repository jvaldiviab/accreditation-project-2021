import Vue from "vue";
import VueRouter from "vue-router";

import Home from "@/components/Home";
import EditIndicator from "@/components/Indicator/EditIndicator";
import DetailIndicator from "@/components/Indicator/DetailIndicator";
import ViewIndicator from "@/components/Indicator/ViewIndicator";
import EditCriteria from "@/components/Criteria/EditCriteria";
import DetailCriteria from "@/components/Criteria/DetailCriteria";
import ViewCriteria from "@/components/Criteria/ViewCriteria";
import EditPhase from "@/components/Phase/EditPhase";
import ViewPhase from "@/components/Phase/ViewPhase";
import DetailPhase from "@/components/Phase/DetailPhase";
import EditProcess from "@/components/Process/EditProcess";
import ListProcess from "@/components/Process/ListProcess";
import NewProcess from "@/components/Process/NewProcess";
import ViewProcess from "@/components/Process/ViewProcess";
import DetailProcess from "@/components/Process/DetailProcess";
import LoginUser from "@/components/Sesions/LoginUser";
import Recover from "@/components/Sesions/Recover";
import ResetPassword from "@/components/Sesions/ResetPassword";
import DeleteTask from "@/components/Tasks/DeleteTask";
import EditTask from "@/components/Tasks/EditTask";
import ListTask from "@/components/Tasks/ListTask";
import NewTask from "@/components/Tasks/NewTask";
import ViewTask from "@/components/Tasks/ViewTask";
import DeleteUser from "@/components/User/DeleteUser";
import DeactivateUser from "@/components/User/DeactivateUser";
import ActivateUser from "@/components/User/ActivateUser";
import EditUser from "@/components/User/EditUser";
import ListUser from "@/components/User/ListUser";
import PerfilUser from "@/components/User/PerfilUser";
import ViewUsers from "@/components/User/ViewUsers";
import RegisterUser from "@/components/User/RegisterUser";
import ProfileUser from "@/components/ProfileUser/ProfileUser";
import ViewProfileUser from "@/components/ProfileUser/ViewProfileUser";
import EditProfileUser from "@/components/ProfileUser/EditProfileUser";
import EditPermissions from "@/components/Roles/EditPermissions";
import ViewRoles from "@/components/Roles/ViewRoles";

import ViewStudyPlan from "@/components/StudyPlan/ViewStudyPlan";
import ListStudyPlan from "@/components/StudyPlan/ListStudyPlan";
import NewStudyPlan from "@/components/StudyPlan/NewStudyPlan";
import DetailStudyPlan from "@/components/StudyPlan/DetailStudyPlan";
import EditStudyPlan from "@/components/StudyPlan/EditStudyPlan";

import ViewCompetence from "@/components/Competence/ViewCompetence";
import ListCompetence from "@/components/Competence/ListCompetence";
import NewCompetence from "@/components/Competence/NewCompetence";
import DetailCompetence from "@/components/Competence/DetailCompetence";
import EditCompetence from "@/components/Competence/EditCompetence";

import ListFormationArea from "@/components/FormationArea/ListFormationArea";
import ViewFormationArea from "@/components/FormationArea/ViewFormationArea";
import NewFormationArea from "@/components/FormationArea/NewFormationArea";
import DetailFormationArea from "@/components/FormationArea/DetailFormationArea";
import EditFormationArea from "@/components/FormationArea/EditFormationArea";


import ListStudentResult from "@/components/StudentResult/ListStudentResult";
import ViewStudentResult from "@/components/StudentResult/ViewStudentResult";
import NewStudentResult from "@/components/StudentResult/NewStudentResult";
import DetailStudentResult from "@/components/StudentResult/DetailStudentResult";
import EditStudentResult from "@/components/StudentResult/EditStudentResult";

import ViewCurriculum from "@/components/Curriculum/ViewCurriculum";
import ListCCurse from "@/components/Curriculum/ListCCurse"
import AddCCurse from "@/components/Curriculum/AddCCurse"
import DetailCCurse from "@/components/Curriculum/DetailCCurse"
import EditCCurse from "@/components/Curriculum/EditCCurse"

import ListSommeliers from "@/components/Sommeliers/ListSommeliers";
import ViewSommeliers from "@/components/Sommeliers/ViewSommeliers";
import NewSommeliers from "@/components/Sommeliers/NewSommeliers";

import ViewMatchCourseVsResults from "@/components/MatchCourseVsResults/ViewMatchCourseVsResults";
import ViewMatchCourseVsCompetence from "@/components/MatchCourseVsCompetence/ViewMatchCourseVsCompetence";
import ViewMatchCompetencesVsResults from "@/components/MatchCompetencesVsResults/ViewMatchCompetencesVsResults";

import ViewCurriculumVitae from "@/components/CurriculumVitae/ViewCurriculumVitae";
import AdminViewCurriculumVitae from "@/components/CurriculumVitae/AdminViewCurriculumVitae";

import ViewSilaboABET from "@/components/Silabo/ViewSilaboABET";

import ViewSilaboDufa from "@/components/SilaboDufa/ViewSilaboDufa";
import ViewEvaluacionSilaboDufa from "@/components/SilaboDufa/ViewEvaluacionSilaboDufa";
import EvaluationAcademicPeriod from "@/components/SilaboDufa/EvaluationAcademicPeriod";
import DeterminarRubricasEvaluacion from "@/components/SilaboDufa/DeterminarRubricasEvaluacion";
import DeterminarCompetencias from "@/components/SilaboDufa/DeterminarCompetencias";
import ViewCompetenciaSilaboDufa from "@/components/SilaboDufa/ViewCompetenciaSilaboDufa";
import ViewMedResultSilaboDufa from "@/components/SilaboDufa/ViewMedResultSilaboDufa";
import DeterminarCriterios from "@/components/SilaboDufa/DeterminarCriterios";

import ExamMenu from "@/components/EntranceExamination/ExamMenu";
import Exam from "@/components/EntranceExamination/Exam";
import Solution from "@/components/EntranceExamination/Solution";
import Report from "@/components/EntranceExamination/Report";

import Fperiod from "@/components/AcademicWork/Fperiod";
import EvaluationReport from "@/components/AcademicWork/EvaluationReport";
import ExamEvidences from "@/components/AcademicWork/ExamEvidences";
import ContinuosEvidences from "@/components/AcademicWork/ContinuosEvidences";
import CourseEvidences from "@/components/AcademicWork/CourseEvidences";

import Speriod from "@/components/AcademicWork/Speriod";
import EvaluationReportSecond from "@/components/AcademicWork/EvaluationReportSecond";
import ExamEvidencesSecond from "@/components/AcademicWork/ExamEvidencesSecond";
import ContinuosEvidencesSecond from "@/components/AcademicWork/ContinuosEvidencesSecond";
import CourseEvidencesSecond from "@/components/AcademicWork/CourseEvidencesSecond";

import Tperiod from "@/components/AcademicWork/Tperiod";
import EvaluationReportThird from "@/components/AcademicWork/EvaluationReportThird";
import ExamEvidencesThird from "@/components/AcademicWork/ExamEvidencesThird";
import ContinuosEvidencesThird from "@/components/AcademicWork/ContinuosEvidencesThird";
import CourseEvidencesThird from "@/components/AcademicWork/CourseEvidencesThird";

import ViewCourses from "@/components/Courses/ViewCourses";
import ListCourses from "@/components/Courses/ListCourses";
import AddCourses from "@/components/Courses/AddCourses";
import EditCourses from "@/components/Courses/EditCourses";
import DeleteCourses from "@/components/Courses/DeleteCourses";
import DeactivateCourse from "@/components/Courses/DeactivateCourse";
import ActivateCourse from "@/components/Courses/ActivateCourse";

import ViewStages from "@/components/Courses/ViewStages";
import Stages from "@/components/Courses/Stages";
import EditStages from "@/components/Courses/EditStages";

import ViewDocentes from "@/components/Courses/ViewDocentes";
import ResponsableCourses from "@/components/Courses/ResponsableCourses";
import EditResponsable from "@/components/Courses/EditResponsable";
import DeleteResponsable from "@/components/Courses/DeleteResponsable";


import AddPortfolio from "@/components/Portfolio/AddPortfolio";
import ListPortfolios from "@/components/Portfolio/ListPortfolios";
import EditPortfolio from "@/components/Portfolio/EditPortfolio";
import DeletePortfolio from "@/components/Portfolio/DeletePortfolio";
import DeactivatePortfolio from "@/components/Portfolio/DeactivatePortfolio";
import ViewPortfolio from "@/components/Portfolio/ViewPortfolio";

import ListSyllabus from "@/components/Courses/ListSyllabus";

import Reports from "@/components/Reports/Reports";

import ViewReportContinuousImprovement from "@/components/ReportContinuousImprovement/ViewReportContinuousImprovement";
import DetailReportContinuousImprovement from "@/components/ReportContinuousImprovement/DetailReportContinuousImprovement";

import ViewReportAcademicAdvisory from "@/components/ReportAcademicAdvisory/ViewReportAcademicAdvisory";
import DetailReportAcademicAdvisory from "@/components/ReportAcademicAdvisory/DetailReportAcademicAdvisory";

import ListStudents from "@/components/Student/ListStudents"
import ViewStudent from "@/components/Student/ViewStudent"

import EditAcademicDevelopment from "@/components/ReportAcademicDevelopment/EditAcademicDevelopment";
import ListAcademicDevelopment from "@/components/ReportAcademicDevelopment/ListAcademicDevelopment";
import NewAcademicDevelopment from "@/components/ReportAcademicDevelopment/NewAcademicDevelopment";
import ViewAcademicDevelopment from "@/components/ReportAcademicDevelopment/ViewAcademicDevelopment";

import ViewReportPerformance from "@/components/ReportPerformance/ViewReportPerformance"
import ReportPerformance from "@/components/ReportPerformance/ReportPerformance"

import ViewReportAverage from "@/components/ReportAverage/ViewReportAverage";
import ListReportAverage from "@/components/ReportAverage/ListReportAverage";

import ViewReportStudentResult from "@/components/ReportStudentResult/ViewReportStudentResult"
import FormReportStudentResult from "@/components/ReportStudentResult/FormReportStudentResult"



Vue.use(VueRouter);

const routes = [
  {
    path: "",
    name: "LoginUser",
    component: LoginUser,
  },
  {
    path: "/",
    name: "Home",
    component: Home,
    children: [
      /*{
        path: "",
        name: "ControlPanel",
        component: ControlPanel,
      },*/

      {
        path: "tasks",
        name: "Task",
        component: ViewTask,
        children: [
          {
            path: "list",
            component: ListTask
          },
          {
            path: "add",
            component: NewTask
          },
          {
            path: ":taskId/edit",
            name: "EditTask",
            component: EditTask,
          },
          {
            path: ":taskId/delete",
            name: "DeleteTask",
            component: DeleteTask,
          },
        ],
      },
      {
        path: "process",
        name: "Process",
        component: ViewProcess,
        children: [
          {
            path: "list",
            name: "ListProcess",
            component: ListProcess
          },
          {
            path: "add",
            name: "NewProcess",
            component: NewProcess
          },
          {
            path: ":processId/detail",
            name: "DetailProcess",
            component: DetailProcess,
          },
          {
            path: ":processId/edit",
            name: "EditProcess",
            component: EditProcess,
          },
        ],
      },

      {
        path: "phase",
        name: "Phase",
        component: ViewPhase,
        children: [
          {
            path: ":phaseId/detail",
            name: "DetailPhase",
            component: DetailPhase,
          },
          {
            path: ":phaseId/edit",
            name: "EditPhase",
            component: EditPhase,
          },
        ],
      },
      {
        path: "criteria",
        name: "Criteria",
        component: ViewCriteria,
        children: [
          {
            path: ":criteriaId/detail",
            name: "DetailCriteria",
            component: DetailCriteria,
          },
          {
            path: ":criteriaId/edit",
            name: "EditCriteria",
            component: EditCriteria,
          },
        ],
      },

      {
        path: "indicator",
        name: "Indicator",
        component: ViewIndicator,
        children: [
          {
            path: ":indicatorId/detail",
            name: "DetailIndicator",
            component: DetailIndicator,
          },
          {
            path: ":indicatorId/edit",
            name: "EditIndicator",
            component: EditIndicator,
          },
        ],
      },

      {
        path: "users",
        name: "Users",
        component: ViewUsers,
        children: [
          {
            path: "list",
            component: ListUser,
            name: "ListUser"
          },
          {
            path: ":userId/edit",
            name: "EditUser",
            component: EditUser
          },
          {
            path: ":userId/perfil",
            name: "PerfilUser",
            component: PerfilUser,
          },
          {
            path: ":userId/delete",
            name: "DeleteUser",
            component: DeleteUser,
          },
          {
            path: ":userId/deactivate",
            name: "DeactivateUser",
            component: DeactivateUser,
          },
          {
            path: ":userId/activate",
            name: "ActivateUser",
            component: ActivateUser,
          },
          {
            path: "register",
            name: "RegisterUser",
            component: RegisterUser,
          },
        ],
      },
      {
        path: "profile",
        name: "Profile",
        component: ViewProfileUser,
        children: [
          {
            path: "/profileuser",
            name: "ProfileUser",
            component: ProfileUser
          },
          {
            path: "/edit",
            name: "EditProfileUser",
            component: EditProfileUser,
          },
        ],
      },
      {
        path: "roles",
        name: "Roles",
        component: ViewRoles,
        children: [
          {
            path: "/editPermissions",
            name: "EditPermissions",
            component: EditPermissions,
          },],
      },
      {
        path: "process/:processId/studyPlan",
        name: "StudyPlan",
        component: ViewStudyPlan,
        children: [
          {
            path: "list",
            name: "ListStudyPlan",
            component: ListStudyPlan
          },
          {
            path: "add",
            name: "NewStudyPlan",
            component: NewStudyPlan
          },
          {
            path: ":studyPlanId/detail",
            name: "DetailStudyPlan",
            component: DetailStudyPlan,
          },
          {
            path: ":studyPlanId/edit",
            name: "EditStudyPlan",
            component: EditStudyPlan
          },
        ],
      },
      {
        path: "process/:processId/studyPlan/:studyPlanId/competence",
        name: "Competence",
        component: ViewCompetence,
        children: [
          {
            path: "list",
            name: "ListCompetence",
            component: ListCompetence
          },
          {
            path: "add",
            name: "NewCompetence",
            component: NewCompetence
          },
          {
            path: ":competenceId/detail",
            name: "DetailCompetence",
            component: DetailCompetence,
          },
          {
            path: ":competenceId/edit",
            name: "EditCompetence",
            component: EditCompetence,
          },
        ],
      },
      {
        path: "process/:processId/studyPlan/:studyPlanId/formationArea",
        name: "FormationArea",
        component: ViewFormationArea,
        children: [
          {
            path: "list",
            name: "ListFormationArea",
            component: ListFormationArea
          },
          {
            path: "add",
            name: "NewFormationArea",
            component: NewFormationArea
          },
          {
            path: ":formationAreaId/detail",
            name: "DetailFormationArea",
            component: DetailFormationArea,
          },
          {
            path: ":formationAreaId/edit",
            name: "EditFormationArea",
            component: EditFormationArea,
          }
        ],
      },
      {
        path: "process/:processId/studyPlan/:studyPlanId/sommeliers",
        name: "Sommeliers",
        component: ViewSommeliers,
        children: [
          {
            path: "list",
            name: "ListSommeliers",
            component: ListSommeliers
          },
          {
            path: "add",
            component: NewSommeliers
          }
        ],
      },
      {
        path: "process/:processId/studyPlan/:studyPlanId/matchCourseVsResults",
        name: "MatchCourseVsResults",
        component: ViewMatchCourseVsResults,
      },
      {
        path: "process/:processId/studyPlan/:studyPlanId/matchCourseVsCompetence",
        name: "MatchCourseVsCompetence",
        component: ViewMatchCourseVsCompetence,
      },
      {
        path: 'process/:processId/studyPlan/:studyPlanId/matchCompetencesVsResults',
        name: 'MatchCompetencesVsResults',
        component: ViewMatchCompetencesVsResults
      },
      {
        path: "process/:processId/studyPlan/:studyPlanId/studentResult",
        name: "StudentResult",
        component: ViewStudentResult,
        children: [
          {
            path: "list",
            name: "ListStudentResult",
            component: ListStudentResult
          },
          {
            path: "add",
            name: "NewStudentResult",
            component: NewStudentResult
          },
          {
            path: ":studentResultId/detail",
            name: "DetailStudentResult",
            component: DetailStudentResult,
          },
          {
            path: ":studentResultId/edit",
            name: "EditStudentResult",
            component: EditStudentResult,
          },
        ],
      },
      {
        path: "process/:processId/studyPlan/:studyPlanId/course",
        name: "curriculum",
        component: ViewCurriculum,
        children: [
          {
            path: "list",
            name: "ListCCurse",
            component: ListCCurse
          },
          {
            path: "add",
            name: "AddCCurse",
            component: AddCCurse
          },
          {
            path: ":ccurseId/detail",
            name: "DetailCCurse",
            component: DetailCCurse,
          },
          {
            path: ":ccurseId/edit",
            name: "EditCCcurse",
            component: EditCCurse,
          },
        ],
      },
      {
        path: "Portfolio/:portfolioId/ViewCourses/:courseId/curriculumVitae/:idUser",
        name: "CurriculumVitae",
        component: ViewCurriculumVitae,
      },
      {
        path: "Portfolio/:portfolioId/ViewCourses/:courseId/curriculumVitaeList",
        name: "CurriculumVitaeList",
        component: AdminViewCurriculumVitae,
      },
      {
        path: "Portfolio/:portfolioId/ViewCourses/:courseId/entranceExamination",
        name: "ExamMenu",
        component: ExamMenu,
        children: [
          {
            path: "exam",
            name: "Exam",
            component: Exam
          },
          {
            path: "solution",
            name: "Solution",
            component: Solution
          },
          {
            path: "report",
            name: "Report",
            component: Report
          },


        ]
      },

      {
        path: "Portfolio/:portfolioId/ViewCourses/:courseId/fperiod",
        name: "Fperiod",
        component: Fperiod,
        children: [
          {
            path: "evaluationReport",
            name: "EvaluationReport",
            component: EvaluationReport,
          },
          {
            path: "examEvidences",
            name: "ExamEvidences",
            component: ExamEvidences,
          },
          {
            path: "continuosEvidences",
            name: "ContinuosEvidences",
            component: ContinuosEvidences,
          },
          {
            path: "courseEvidences",
            name: "CourseEvidences",
            component: CourseEvidences,
          },

        ]
      },

      {
        path: "Portfolio/:portfolioId/ViewCourses/:courseId/speriod",
        name: "Speriod",
        component: Speriod,
        children: [
          {
            path: "evaluationReportSecond",
            name: "EvaluationReportSecond",
            component: EvaluationReportSecond,
          },
          {
            path: "examEvidencesSecond",
            name: "ExamEvidencesSecond",
            component: ExamEvidencesSecond,
          },
          {
            path: "continuosEvidencesSecond",
            name: "ContinuosEvidencesSecond",
            component: ContinuosEvidencesSecond,
          },
          {
            path: "courseEvidencesSecond",
            name: "CourseEvidencesSecond",
            component: CourseEvidencesSecond,
          },
        ]
      },

      {
        path: "Portfolio/:portfolioId/ViewCourses/:courseId/tperiod",
        name: "Tperiod",
        component: Tperiod,
        children: [
          {
            path: "evaluationReportThird",
            name: "EvaluationReportThird",
            component: EvaluationReportThird,
          },
          {
            path: "examEvidencesThird",
            name: "ExamEvidencesThird",
            component: ExamEvidencesThird,
          },
          {
            path: "continuosEvidencesThird",
            name: "ContinuosEvidencesThird",
            component: ContinuosEvidencesThird,
          },
          {
            path: "courseEvidencesThird",
            name: "CourseEvidencesThird",
            component: CourseEvidencesThird,
          },
        ]
      },

      {
        path: "Portfolio/:portfolioId/ViewCourses/:courseId/silaboABET",
        name: "SilaboABET",
        component: ViewSilaboABET,
      },
      {
        path: "Portfolio/:portfolioId/ViewCourses/",
        name: "viewCourses",
        component: ViewCourses,
        children: [
          {
            path: "ListCourses",
            name: "listCourses",
            component: ListCourses,
          },
          {
            path: "AddCourses",
            name: "AddCourses",
            component: AddCourses,
          },
          {
            path: ":courseId/EditCourses",
            name: "editCourses",
            component: EditCourses,
          },
          {
            path: ":courseId/DeleteCourses",
            name: "deleteCourses",
            component: DeleteCourses,
          },
          {
            path: ":courseId/DeactivateCourse",
            name: "deactivateCourse",
            component: DeactivateCourse,
          },
          {
            path: ":courseId/ActivateCourse",
            name: "activateCourse",
            component: ActivateCourse,
          },


        ]
      },

      {
        path: "Portfolio/:portfolioId/viewStages",
        name: "viewStages",
        component: ViewStages,
        children: [
          {
            path: ":courseId/Stages",
            name: "stages",
            component: Stages,
          },
          {
            path: ":courseId/EditStages",
            name: "editStages",
            component: EditStages,
          },

        ]
      },

      {
        path: "Portfolio/:portfolioId/viewDocentes",
        name: "viewDocentes",
        component: ViewDocentes,
        children: [
          {
            path: ":courseId/ResponsableCourses",
            name: "responsableCourses",
            component: ResponsableCourses,
          },
          {
            path: ":courseId/EditResponsable",
            name: "editResponsable",
            component: EditResponsable,
          },
          {
            path: ":courseId/DeleteResponsable",
            name: "deleteResponsable",
            component: DeleteResponsable,
          },
        ]
      },

      {
        path: "Portfolio/:portfolioId/students",
        name: "ListStudents",
        component: ViewStudent,
        children: [
          {
            path: ":courseId/list",
            name: "ListStudents",
            component: ListStudents
          }]
      },
      {
        path: "ViewPortfolio",
        name: "viewPortfolio",
        component: ViewPortfolio,
        children: [
          {
            path: "AddPortfolio",
            name: "addPortfolio",
            component: AddPortfolio,
          },
          {
            path: "ListPortfolios",
            name: "listPortfolios",
            component: ListPortfolios,
          },
          {
            path: ":portfolioId/EditPortfolio",
            name: "editPortfolio",
            component: EditPortfolio,
          },
          {
            path: "DeletePortfolio",
            name: "deletePortfolio",
            component: DeletePortfolio,
          },
          {
            path: "DeactivatePortfolio",
            name: "deactivatePortfolio",
            component: DeactivatePortfolio,
          },
        ]
      },



      {
        path: "Portfolio/:portfolioId/ViewCourses/:courseId/ListSyllabus",
        name: "listSyllabus",
        component: ListSyllabus,
      },
      {
        path: "Portfolio/:portfolioId/ViewCourses/:courseId/Reports",
        name: "Reports",
        component: Reports,
      },
      {
        path: "Portfolio/:portfolioId/ViewCourses/:courseId/Reports/Performance",
        name: "Performance",
        component: ViewReportPerformance,
      },
      {
        path: "Portfolio/:portfolioId/ViewCourses/:courseId/Reports/Performance/report",
        name: "ReportPerformance",
        component: ReportPerformance,
      },
      {
        path: "Portfolio/:portfolioId/ViewCourses/:courseId/Reports/ContinuousImprovement",
        name: "ContinuousImprovement",
        component: ViewReportContinuousImprovement,
        children: [
          {
            path: "",
            name: "DetailReportContinuousImprovement",
            component: DetailReportContinuousImprovement,
          },],
      },
      {
        path: "Portfolio/:portfolioId/ViewCourses/:courseId/Reports/AcademicAdvisory",
        name: "AcademicAdvisory",
        component: ViewReportAcademicAdvisory,
        children: [
          {
            path: "",
            name: "DetailReportAcademicAdvisory",
            component: DetailReportAcademicAdvisory,
          }],
      },
      {
        path: "Portfolio/:portfolioId/ViewCourses/:courseId/Reports/Average",
        name: "Average",
        component: ViewReportAverage,
        children: [
          {
            path: "",
            name: "ListReportAverage",
            component: ListReportAverage,
          },
        ],
      },
      {
        path: "Portfolio/:portfolioId/ViewCourses/:courseId/Reports/StudentResult",
        name: "ViewReportStudentResult",
        component: ViewReportStudentResult,
      },
      {
        path: "Portfolio/:portfolioId/ViewCourses/:courseId/Reports/StudentResult/Form/:academicPeriodId",
        name: "FormReportStudentResult",
        component: FormReportStudentResult,
      },
      {
        path: "Portfolio/:portfolioId/ViewCourses/:courseId/Reports/AcademicDevelopment",
        name: "AcademicDevelopment",
        component: ViewAcademicDevelopment,
        children: [
          {
            path: "",
            name: "ListAcademicDevelopment",
            component: ListAcademicDevelopment
          },
          {
            path: "add",
            name: "NewAcademicDevelopment",
            component: NewAcademicDevelopment
          },
          {
            path: ":idAD/edit",
            name: "EditAcademicDevelopment",
            component: EditAcademicDevelopment,
          },
        ],
      },
      {
        path: "Portfolio/:portfolioId/:courseId/silaboDufa",
        name: "SilaboDufa",
        component: ViewSilaboDufa,
        children: [
          {
            path: "viewEvaluacionSilaboDufa",
            name: "ViewEvaluacionSilaboDufa",
            component: ViewEvaluacionSilaboDufa,
            children: [
              {
                path: "evaluationAcademicPeriod/:academicPeriodId",
                name: "EvaluationAcademicPeriod",
                component: EvaluationAcademicPeriod,
                children: [
                  {
                    path: "determinarRubricasEvaluacion",
                    name: "DeterminarRubricasEvaluacion",
                    component: DeterminarRubricasEvaluacion
                  },
                  {
                    path: "determinarCompetencias",
                    name: "DeterminarCompetencias",
                    component: DeterminarCompetencias
                  },
                  {
                    path: "determinarCriterios",
                    name: "DeterminarCriterios",
                    component: DeterminarCriterios
                  },

                ]
              },
            ]
          },
          {
            path: "viewCompetenciaSilaboDufa",
            name: "ViewCompetenciaSilaboDufa",
            component: ViewCompetenciaSilaboDufa,
            children: [

            ]
          },
          {
            path: "viewMedResultSilaboDufa",
            name: "ViewMedResultSilaboDufa",
            component: ViewMedResultSilaboDufa,
            children: [

            ]
          },
        ],
      },

    ],
  },

  {
    path: "/login",
    name: "LoginUser",
    component: LoginUser,
  },
  {
    path: "/recover",
    name: "Recover",
    component: Recover,
  },
  {
    path: "/ResetPassword/:uidb64/:tokenReset",
    name: "ResetPassword",
    component: ResetPassword,
  },
  {
    path: "/process/new",
    name: "NewProcess",
    component: NewProcess,
  },

  {
    path: "/criteria/:criteriaId/edit",
    name: "EditCriteria",
    component: EditCriteria,
  },

  {
    path: "/indicator/:indicatorId/edit",
    name: "EditIndicator",
    component: EditIndicator,
  },

];

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes,
});

export default router;