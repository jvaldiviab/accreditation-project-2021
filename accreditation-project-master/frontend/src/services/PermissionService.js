//import Vue from 'vue'
import store from '../store';
import axios from "axios";
const url = process.env.VUE_APP_RUTA_API;
import AuthService from "@/services/AuthService.js";
//Vue.store.getters.appSettings

//store.getters.appSettings
    
    
export const PermissionService = {
    //findPermission,
    //getById

    /**
         * @param {number} a Permiso ingresado
         * @param {number} b Modulo ingresado
         * @description Verifica si existe el permiso ingresado en los argumentos en la variable userPermissions
         * @return {boolean}
         */
    findPermission(a, b) {
        
    
        if (store.getters.isSuperadmin) 
            return true;
          
        return store.getters.getPermisos.some(
          (x) => x.permission == a && x.module == b
        );

    
        // return true;
  },
  
  /**
     * @description Obtiene los permisos del usuario logueado
     */
    async setUserActive() {
      //console.log("estoyyy en setUserActive")

      const response = await AuthService.getUser();
      try {
        const superadmin = response.superadmin;
        store.dispatch("superadmin", { superadmin });
        //store.commit('SET_SUPERADMIN', superadmin)
      } catch (error) {
        const superadmin = false;
        store.dispatch("superadmin", { superadmin });
        //store.commit('SET_SUPERADMIN', superadmin)
      }

      const path = url + `user/active/`;
      axios
        .get(path)
        .then((response) => {
          const p = response.data.role.permissions;
          store.dispatch("permisos", { p });

          //store.dispatch("idProceso", response.data.role.id_process);
          //store.commit('SET_PERMISOS',  p )
          store.commit('SET_IDPROCESO', response.data.role.id_process)
        })
        .catch((error) => {
          console.log(error);
        });

        ///responsable de algun proceso
        const patha = url + 'process/process/?id_user='+store.getters.getId;

        await axios.get(patha).then((response) => {
          this.procesos = response.data;
  
        if(this.procesos.length > 0){
          store.commit('SET_ISRESPPROCESO', true)
        }else{
          store.commit('SET_ISRESPPROCESO', false)
        }
  
        }).catch((error) => {
          console.log(error);
          store.commit('SET_ISRESPPROCESO', false)
          
          
        });

      ///responsable de algun portafolio
        const paths = url + `process/portfolio/portfolio/`;

        await axios.get(paths).then((response) => {
          this.portfolios = response.data;
          const listResp = this.portfolios.filter(tmp => tmp.id_user == store.getters.getId);
  
        if(listResp.length > 0){

          store.commit('SET_ISRESPPORTCURSO', true)
        }else{

          store.commit('SET_ISRESPPORTCURSO', false)
        }

        //verificar si es colaborador de algun porcurso
        const listRespC = this.portfolios.filter(tmp => tmp.users.some(
          (x) => x ==store.getters.getId
          
          ));

          if(listRespC.length > 0){
            //return true;
            store.commit('SET_ISCOLABPORTCURSO', true)
          }else{
            //return false;
            store.commit('SET_ISCOLABPORTCURSO', false)
          }

  
        }).catch((error) => {
          console.log(error);

          store.commit('SET_ISRESPPORTCURSO', false)
          store.commit('SET_ISCOLABPORTCURSO', false)
        });


        //responsable de curso activo [docente]

        const pathaa = url + 'process/portfolio/course_active_user/?id_user='+store.getters.getId;

        await axios.get(pathaa).then((response) => {
          this.procesos = response.data;
  
        if(this.procesos.length > 0){
          store.commit('SET_ISRESPCURSOACT', true)
        }else{
          store.commit('SET_ISRESPCURSOACT', false)
        }
  
        }).catch((error) => {
          console.log(error);
          store.commit('SET_ISRESPCURSOACT', false)
          
          
        });

    },

    //obtener el id del portafolio don el usuario es responsablde un curso activo
    async getIDPortafolioRespCA() {
      const path = url + 'process/portfolio/course_active_user/?id_user='+store.getters.getId;

      return await axios.get(path).then((response) => {
        this.tmp = response.data;

      if(this.tmp.length > 0){
        let idCA= this.tmp[0].id_course_active;


        let pathx = url + 'process/portfolio/course_active/'+idCA+'/';
        let idPortafolio = axios.get(pathx).then((response) => {
          this.tmp = response.data;

          return response.data.id_portfolio;
  

        }).catch((error) => {
          console.log(error);
          return -1;
        });




        return idPortafolio;
      }else{
        return -1;
      }

      }).catch((error) => {
        console.log(error);
        return -1;
      });


  },

//No seusan actualmete
      /**
     * @description verifica el usuario logueado es responsable de un portafolio
     */
    async isRespPortC() {
      const path = url + `process/portfolio/portfolio/`;

      return await axios.get(path).then((response) => {
        this.portfolios = response.data;

        //console.log(this.portfolios);

        const listResp = this.portfolios.filter(tmp => tmp.id_user == store.getters.getId);

      if(listResp.length > 0){
        return true;
      }else{
        return false;
      }

      }).catch((error) => {
        console.log(error);
        return false;
      });

    },
      /**
     * @description verifica el usuario logueado es colaborador de un portafolio
     */
    async isColabPortC() {
      const path = url + `process/portfolio/portfolio/`;

      return await axios.get(path).then((response) => {
        this.portfolios = response.data;

        //console.log(this.portfolios);

        const listResp = this.portfolios.filter(tmp => tmp.users.some(
          (x) => x ==store.getters.getId
          
          ));

        //console.log("soy colaborador:>>> "+listResp.length)
      if(listResp.length > 0){
        return true;
      }else{
        return false;
      }

      }).catch((error) => {
        console.log(error);
        return false;
      });
    },
      /**
     * @description verifica el usuario logueado es responsable de un proceso
     */
    async isRespProcesos() {
      const path = url + 'process/process/?id_user='+store.getters.getId;

      return await axios.get(path).then((response) => {
        this.procesos = response.data;


      console.log("soy vv proceso:>>> "+this.procesos.length)
      if(this.procesos.length > 0){
        return true;
      }else{
        return false;
      }

      }).catch((error) => {
        console.log(error);
        return false;
      });


    },

   
    
};



