import store from '../store';
import axios from "axios";
const url = process.env.VUE_APP_RUTA_API;
//import AuthService from "@/services/AuthService.js";
import {
  Module,
  Permission,
  PermissionService,
} from "@/services"; 
export const CourseDatesService = {


  /**
         * @param {number} portfolioId ID del portafolio del que se obtendran las fechas
         * @description Actualiza las variables persistentes con las fechas que contiene un portafolio
         */
  async getPortfolioDates(portfolioId) {
    const path = url + `process/portfolio/portfolio/${portfolioId}/`;
      await axios.get(path).then((response) => {
        //const today = new Date(Date.now());
        const fechaActual = new Date(Date.now());
        const fechaInicio = new Date(response.data.start_date);
        const fechaFinFase1 = new Date(response.data.end_phase1);
        const fechaFinFase2 = new Date(response.data.end_phase2);
        const fechaFinFase3 = new Date(response.data.end_phase3);
        const fechaFinFase4 = new Date(response.data.end_phase4);
        const usuarios = response.data.users;
        
        store.dispatch("fechaActual", { fechaActual });
        store.dispatch("fechaInicio", { fechaInicio });
        store.dispatch("fechaFinFase1", { fechaFinFase1 });
        store.dispatch("fechaFinFase2", { fechaFinFase2 });
        store.dispatch("fechaFinFase3", { fechaFinFase3 });
        store.dispatch("fechaFinFase4", { fechaFinFase4 });
        store.dispatch("usuarios", { usuarios });
      });
    
    
    //console.log(store.getters.getFechaActual);
    //console.log(store.getters.getUsuarios);
    //console.log(store.getters.getId);
  },
  
  /**
    * @description Verifica si un usuario es super administrador, tiene permisos, es responsale de proceso o de portafolio
    * @return {boolean}
  */
  isUserSeeEveything() {
    if (store.getters.isSuperadmin) {
      return true;
    }
    if (PermissionService.findPermission(Permission.View, Module.Portafolio)) {
      return true;
    }
    if (store.getters.isResProceso) {
      return true;
    }
    if (store.getters.isRespPortCurso) {
      return true;
    }
    
    return false;
  },

  /**
    * @description Verifica si un usuario es colaborador de portafolio o responsable de un curso activo
    * @return {boolean}
  */
  isUserColab() {
    if (store.getters.isColabPortCurso) {
       return true;
    }
    if ( store.getters.isRespCursoAct ){
      return true;
    }
    
    return false;
  },

  /**
    * @description Verifica si la fecha actual esta dentro del perido de la primera fase de acuerdo a las 
    * variables persistentes
    * @return {boolean}
  */
  isFirstPhase() { 
    return store.getters.getFechaInicio <= store.getters.getFechaActual &&
      store.getters.getFechaActual <= store.getters.getFechaFinFase1
  },
  /**
    * @description Verifica si la fecha actual esta dentro del perido de la segunda fase de acuerdo a las 
    * variables persistentes
    * @return {boolean}
  */
  isSecondPhase() {
    return store.getters.getFechaFinFase1 <= store.getters.getFechaActual &&
      store.getters.getFechaActual <= store.getters.getFechaFinFase2
  },
  /**
    * @description Verifica si la fecha actual esta dentro del perido de la tercera fase de acuerdo a las 
    * variables persistentes
    * @return {boolean}
  */
  isThirdPhase() {
    return store.getters.getFechaFinFase2 <= store.getters.getFechaActual &&
      store.getters.getFechaActual <= store.getters.getFechaFinFase3
  },
  /**
    * @description Verifica si la fecha actual esta dentro del perido de la cuarta fase de acuerdo a las 
    * variables persistentes
    * @return {boolean}
  */
  isFourthPhase() {
    return store.getters.getFechaFinFase3 <= store.getters.getFechaActual &&
      store.getters.getFechaActual <= store.getters.getFechaFinFase4
  },
};
